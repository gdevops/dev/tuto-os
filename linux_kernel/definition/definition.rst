

.. index::
   pair: Linux Kernel ; Definition


.. _linux_kernel_definition:

==========================================
GNU/Linux kernel definition
==========================================

.. seealso::

   - https://en.wikipedia.org/wiki/Linux_kernel


.. contents::
   :depth: 3

Wikipedia definition
=====================

The Linux kernel is a free and open-source, monolithic, Unix-like operating
system kernel.
The Linux family of operating systems is based on this kernel and deployed
on both traditional computer systems such as personal computers and servers,
usually in the form of Linux distributions, and on various embedded devices
such as routers, wireless access points, PBXes, set-top boxes, FTA receivers,
smart TVs, PVRs, and NAS appliances.

While the adoption of the Linux kernel in desktop computer operating system
is low, Linux-based operating systems dominate nearly every other segment of
computing, from mobile devices to mainframes.

As of November 2017, all of the world's 500 most powerful supercomputers
run Linux.

The Android operating system for tablet computers, smartphones, and smartwatches
also uses the Linux kernel.
