

.. _linux_commands:

==========================================
Commands
==========================================


.. toctree::
   :maxdepth: 3

   journalctl/journalctl
   systemctl/systemctl
