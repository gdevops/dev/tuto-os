

.. _systemctl:

==========================================
systemctl
==========================================


Debian systemd
===============

.. seealso::

   - https://wiki.debian.org/fr/systemd



systemctl
=============

::

    systemctl


::


    UNIT                                                                                                        LOAD   ACTIVE SUB       DESCRIPTION
    proc-sys-fs-binfmt_misc.automount                                                                           loaded active running   Arbitrary Executable File Formats File System Automount Point
    sys-devices-pci0000:00-0000:00:14.0-usb1-1\x2d14-1\x2d14:1.0-bluetooth-hci0.device                          loaded active plugged   /sys/devices/pci0000:00/0000:00:14.0/usb1/1-14/1-14:1.0/bluetooth/hci0
    sys-devices-pci0000:00-0000:00:16.3-tty-ttyS0.device                                                        loaded active plugged   Cannon Lake PCH Active Management Technology - SOL
    sys-devices-pci0000:00-0000:00:1b.0-0000:01:00.0-nvme-nvme0-nvme0n1-nvme0n1p1.device                        loaded active plugged   CT500P1SSD8 1
    sys-devices-pci0000:00-0000:00:1b.0-0000:01:00.0-nvme-nvme0-nvme0n1-nvme0n1p2.device                        loaded active plugged   CT500P1SSD8 2
    sys-devices-pci0000:00-0000:00:1b.0-0000:01:00.0-nvme-nvme0-nvme0n1-nvme0n1p3.device                        loaded active plugged   LVM PV 0v4cl4-PeOc-wfk8-rb1h-wJUE-bXU5-XbNWY4 on /dev/nvme0n1p3 3
    sys-devices-pci0000:00-0000:00:1b.0-0000:01:00.0-nvme-nvme0-nvme0n1.device                                  loaded active plugged   CT500P1SSD8
    sys-devices-pci0000:00-0000:00:1f.3-sound-card0.device                                                      loaded active plugged   Cannon Lake PCH cAVS
    sys-devices-pci0000:00-0000:00:1f.6-net-eno1.device                                                         loaded active plugged   Ethernet Connection (7) I219-LM
    sys-devices-platform-serial8250-tty-ttyS1.device                                                            loaded active plugged   /sys/devices/platform/serial8250/tty/ttyS1
    sys-devices-platform-serial8250-tty-ttyS2.device                                                            loaded active plugged   /sys/devices/platform/serial8250/tty/ttyS2
    sys-devices-platform-serial8250-tty-ttyS3.device                                                            loaded active plugged   /sys/devices/platform/serial8250/tty/ttyS3
    sys-devices-virtual-block-dm\x2d0.device                                                                    loaded active plugged   /sys/devices/virtual/block/dm-0
    sys-devices-virtual-block-dm\x2d1.device                                                                    loaded active plugged   /sys/devices/virtual/block/dm-1
    sys-devices-virtual-misc-rfkill.device                                                                      loaded active plugged   /sys/devices/virtual/misc/rfkill
    sys-devices-virtual-net-br\x2d4b24804ed7b9.device                                                           loaded active plugged   /sys/devices/virtual/net/br-4b24804ed7b9
    sys-devices-virtual-net-br\x2d58b09a6abd85.device                                                           loaded active plugged   /sys/devices/virtual/net/br-58b09a6abd85
    sys-devices-virtual-net-docker0.device                                                                      loaded active plugged   /sys/devices/virtual/net/docker0
    sys-devices-virtual-net-ppp0.device                                                                         loaded active plugged   /sys/devices/virtual/net/ppp0
    sys-devices-virtual-net-veth213cb95.device                                                                  loaded active plugged   /sys/devices/virtual/net/veth213cb95
    sys-module-fuse.device                                                                                      loaded active plugged   /sys/module/fuse
    sys-subsystem-bluetooth-devices-hci0.device                                                                 loaded active plugged   /sys/subsystem/bluetooth/devices/hci0
    sys-subsystem-net-devices-br\x2d4b24804ed7b9.device                                                         loaded active plugged   /sys/subsystem/net/devices/br-4b24804ed7b9
    sys-subsystem-net-devices-br\x2d58b09a6abd85.device                                                         loaded active plugged   /sys/subsystem/net/devices/br-58b09a6abd85
    sys-subsystem-net-devices-docker0.device                                                                    loaded active plugged   /sys/subsystem/net/devices/docker0
    sys-subsystem-net-devices-eno1.device                                                                       loaded active plugged   Ethernet Connection (7) I219-LM
    sys-subsystem-net-devices-ppp0.device                                                                       loaded active plugged   /sys/subsystem/net/devices/ppp0
    sys-subsystem-net-devices-veth213cb95.device                                                                loaded active plugged   /sys/subsystem/net/devices/veth213cb95
    -.mount                                                                                                     loaded active mounted   Root Mount
    boot-efi.mount                                                                                              loaded active mounted   /boot/efi
    boot.mount                                                                                                  loaded active mounted   /boot
    dev-hugepages.mount                                                                                         loaded active mounted   Huge Pages File System
    dev-mqueue.mount                                                                                            loaded active mounted   POSIX Message Queue File System
    proc-sys-fs-binfmt_misc.mount                                                                               loaded active mounted   Arbitrary Executable File Formats File System
    run-docker-netns-ae3f2b97a273.mount                                                                         loaded active mounted   /run/docker/netns/ae3f2b97a273
    run-user-1001-gvfs.mount                                                                                    loaded active mounted   /run/user/1001/gvfs
    run-user-1001.mount                                                                                         loaded active mounted   /run/user/1001
    sys-fs-fuse-connections.mount                                                                               loaded active mounted   FUSE Control File System
    sys-kernel-debug.mount                                                                                      loaded active mounted   Kernel Debug File System
    var-lib-docker-containers-d3dbe6e269b253fd4d897d1677dfc2abb4e22765a3125e0ebcdce7b407f6e36f-mounts-shm.mount loaded active mounted   /var/lib/docker/containers/d3dbe6e269b253fd4d897d1677dfc2abb4e22765a3125e0ebcd
    var-lib-docker-overlay2-70abb610fc173048bbb0049629ca3c0f998f6f330b2c20d4c928a6607675cc53-merged.mount       loaded active mounted   /var/lib/docker/overlay2/70abb610fc173048bbb0049629ca3c0f998f6f330b2c20d4c928a
    cups.path                                                                                                   loaded active running   CUPS Scheduler
    systemd-ask-password-plymouth.path                                                                          loaded active waiting   Forward Password Requests to Plymouth Directory Watch
    systemd-ask-password-wall.path                                                                              loaded active waiting   Forward Password Requests to Wall Directory Watch
    init.scope                                                                                                  loaded active running   System and Service Manager
    session-2.scope                                                                                             loaded active running   Session 2 of user pvergain
    accounts-daemon.service                                                                                     loaded active running   Accounts Service
    alsa-restore.service                                                                                        loaded active exited    Save/Restore Sound Card State
    alsa-state.service                                                                                          loaded active running   Manage Sound Card State (restore and store)
    apache2.service                                                                                             loaded active running   The Apache HTTP Server
    apparmor.service                                                                                            loaded active exited    Load AppArmor profiles
    atd.service                                                                                                 loaded active running   Deferred execution scheduler
    avahi-daemon.service                                                                                        loaded active running   Avahi mDNS/DNS-SD Stack
    binfmt-support.service                                                                                      loaded active exited    Enable support for additional executable binary formats
    blk-availability.service                                                                                    loaded active exited    Availability of block devices
    bluetooth.service                                                                                           loaded active running   Bluetooth service
    colord.service                                                                                              loaded active running   Manage, Install and Generate Color Profiles
    console-setup.service                                                                                       loaded active exited    Set console font and keymap
    containerd.service                                                                                          loaded active running   containerd container runtime
    cron.service                                                                                                loaded active running   Regular background program processing daemon
    cups-browsed.service                                                                                        loaded active running   Make remote CUPS printers available locally
    cups.service                                                                                                loaded active running   CUPS Scheduler
    dbus.service                                                                                                loaded active running   D-Bus System Message Bus
    docker.service                                                                                              loaded active running   Docker Application Container Engine
    exim4.service                                                                                               loaded active running   LSB: exim Mail Transport Agent
    getty@tty1.service                                                                                          loaded active running   Getty on tty1
    glances.service                                                                                             loaded active running   Glances
    hddtemp.service                                                                                             loaded active exited    LSB: disk temperature monitoring daemon
    ifupdown-pre.service                                                                                        loaded active exited    Helper to synchronize boot up for ifupdown
    keyboard-setup.service                                                                                      loaded active exited    Set the console keyboard layout
    kmod-static-nodes.service                                                                                   loaded active exited    Create list of required static device nodes for the current kernel
    lightdm.service                                                                                             loaded active running   Light Display Manager
    lm-sensors.service                                                                                          loaded active exited    Initialize hardware monitoring sensors
    lvm2-monitor.service                                                                                        loaded active exited    Monitoring of LVM2 mirrors, snapshots etc. using dmeventd or progress polling
    lvm2-pvscan@259:3.service                                                                                   loaded active exited    LVM event activation on device 259:3
    mariadb.service                                                                                             loaded active running   MariaDB 10.3.27 database server
    memcached.service                                                                                           loaded active running   memcached daemon
    ModemManager.service                                                                                        loaded active running   Modem Manager
    networking.service                                                                                          loaded active exited    Raise network interfaces
    NetworkManager-wait-online.service                                                                          loaded active exited    Network Manager Wait Online
    NetworkManager.service                                                                                      loaded active running   Network Manager
    polkit.service                                                                                              loaded active running   Authorization Manager
    postgresql.service                                                                                          loaded active exited    PostgreSQL RDBMS
    postgresql@13-main.service                                                                                  loaded active running   PostgreSQL Cluster 13-main
    rsyslog.service                                                                                             loaded active running   System Logging Service
    rtkit-daemon.service                                                                                        loaded active running   RealtimeKit Scheduling Policy Service
    ssh.service                                                                                                 loaded active running   OpenBSD Secure Shell server
    sysstat.service                                                                                             loaded active exited    Resets System Activity Data Collector
    systemd-fsck@dev-disk-by\x2duuid-11E3\x2dAF81.service                                                       loaded active exited    File System Check on /dev/disk/by-uuid/11E3-AF81
    systemd-fsck@dev-disk-by\x2duuid-9af703bf\x2dcefc\x2d4d90\x2dbd57\x2d084297e0dccf.service                   loaded active exited    File System Check on /dev/disk/by-uuid/9af703bf-cefc-4d90-bd57-084297e0dccf
    systemd-journal-flush.service                                                                               loaded active exited    Flush Journal to Persistent Storage
    systemd-journald.service                                                                                    loaded active running   Journal Service
    systemd-logind.service                                                                                      loaded active running   Login Service
    systemd-modules-load.service                                                                                loaded active exited    Load Kernel Modules
    systemd-random-seed.service                                                                                 loaded active exited    Load/Save Random Seed
    systemd-remount-fs.service                                                                                  loaded active exited    Remount Root and Kernel File Systems
    systemd-sysctl.service                                                                                      loaded active exited    Apply Kernel Variables
    systemd-sysusers.service                                                                                    loaded active exited    Create System Users
    systemd-timesyncd.service                                                                                   loaded active running   Network Time Synchronization
    systemd-tmpfiles-setup-dev.service                                                                          loaded active exited    Create Static Device Nodes in /dev
    systemd-tmpfiles-setup.service                                                                              loaded active exited    Create Volatile Files and Directories
    systemd-udev-trigger.service                                                                                loaded active exited    udev Coldplug all Devices
    systemd-udevd.service                                                                                       loaded active running   udev Kernel Device Manager
    systemd-update-utmp.service                                                                                 loaded active exited    Update UTMP about System Boot/Shutdown
    systemd-user-sessions.service                                                                               loaded active exited    Permit User Sessions


systemctl get-default
======================

::

    systemctl get-default

::

    graphical.target


systemctl show -p Wants -p Requires graphical.target
=========================================================

::

    systemctl show -p Wants -p Requires graphical.target

::

    Requires=multi-user.target
    Wants=systemd-update-utmp-runlevel.service exim4.service lightdm.service accounts-daemon.service udisks2.service hddtemp.service


systemctl status
===================

::

    systemctl status

::

    ● uc045
        State: running
         Jobs: 0 queued
       Failed: 0 units
        Since: Wed 2021-04-28 05:53:39 CEST; 33min ago
       CGroup: /
               ├─user.slice
               │ └─user-1001.slice
               │   ├─session-2.scope
               │   │ ├─ 1676 lightdm --session-child 12 21
               │   │ ├─ 1694 /usr/bin/gnome-keyring-daemon --daemonize --login
               │   │ ├─ 1697 cinnamon-session --session cinnamon
               │   │ ├─ 1725 /usr/bin/ssh-agent cinnamon-session-cinnamon


systemctl list-units
=======================

::

    systemctl list-units

::

    UNIT                                                                                      LOAD   ACTIVE SUB       DESCRIPTION
    proc-sys-fs-binfmt_misc.automount                                                         loaded active running   Arbitrary Executable File Formats File System Automount Point
    sys-devices-pci0000:00-0000:00:14.0-usb1-1\x2d14-1\x2d14:1.0-bluetooth-hci0.device        loaded active plugged   /sys/devices/pci0000:00/0000:00:14.0/usb1/1-14/1-14:1.0/bluetooth/hci0
    sys-devices-pci0000:00-0000:00:16.3-tty-ttyS0.device                                      loaded active plugged   Cannon Lake PCH Active Management Technology - SOL
    sys-devices-pci0000:00-0000:00:1b.0-0000:01:00.0-nvme-nvme0-nvme0n1-nvme0n1p1.device      loaded active plugged   CT500P1SSD8 1

systemctl list-unit-files
============================

::

    systemctl list-unit-files

::

    UNIT FILE                                  STATE
    proc-sys-fs-binfmt_misc.automount          static
    -.mount                                    generated
    boot-efi.mount                             generated
    boot.mount                                 generated
    dev-hugepages.mount                        static
    dev-mqueue.mount                           static
    proc-sys-fs-binfmt_misc.mount              static
    sys-fs-fuse-connections.mount              static
    sys-kernel-config.mount                    static
    sys-kernel-debug.mount                     static


systemctl list-units --type service
======================================

::

    systemctl list-units --type service

::

    UNIT                                                                                      LOAD   ACTIVE SUB     DESCRIPTION
    accounts-daemon.service                                                                   loaded active running Accounts Service
    alsa-restore.service                                                                      loaded active exited  Save/Restore Sound Card State
    alsa-state.service                                                                        loaded active running Manage Sound Card State (restore and store)
    apache2.service                                                                           loaded active running The Apache HTTP Server
    apparmor.service                                                                          loaded active exited  Load AppArmor profiles
    atd.service                                                                               loaded active running Deferred execution scheduler
    avahi-daemon.service                                                                      loaded active running Avahi mDNS/DNS-SD Stack
    binfmt-support.service                                                                    loaded active exited  Enable support for additional executable binary formats
    blk-availability.service                                                                  loaded active exited  Availability of block devices
    bluetooth.service                                                                         loaded active running Bluetooth service
    colord.service                                                                            loaded active running Manage, Install and Generate Color Profiles
    console-setup.service                                                                     loaded active exited  Set console font and keymap
    containerd.service                                                                        loaded active running containerd container runtime
    cron.service                                                                              loaded active running Regular background program processing daemon
    cups-browsed.service                                                                      loaded active running Make remote CUPS printers available locally
    cups.service                                                                              loaded active running CUPS Scheduler
    dbus.service                                                                              loaded active running D-Bus System Message Bus
    docker.service                                                                            loaded active running Docker Application Container Engine
    exim4.service                                                                             loaded active running LSB: exim Mail Transport Agent
    getty@tty1.service                                                                        loaded active running Getty on tty1
    glances.service                                                                           loaded active running Glances
    hddtemp.service                                                                           loaded active exited  LSB: disk temperature monitoring daemon
    ifupdown-pre.service                                                                      loaded active exited  Helper to synchronize boot up for ifupdown
    keyboard-setup.service                                                                    loaded active exited  Set the console keyboard layout
    kmod-static-nodes.service                                                                 loaded active exited  Create list of required static device nodes for the current kernel
    lightdm.service                                                                           loaded active running Light Display Manager
    lm-sensors.service                                                                        loaded active exited  Initialize hardware monitoring sensors
    lvm2-monitor.service                                                                      loaded active exited  Monitoring of LVM2 mirrors, snapshots etc. using dmeventd or progress polling
    lvm2-pvscan@259:3.service                                                                 loaded active exited  LVM event activation on device 259:3
    mariadb.service                                                                           loaded active running MariaDB 10.3.27 database server
    memcached.service                                                                         loaded active running memcached daemon
    ModemManager.service                                                                      loaded active running Modem Manager
    networking.service                                                                        loaded active exited  Raise network interfaces
    NetworkManager-wait-online.service                                                        loaded active exited  Network Manager Wait Online
    NetworkManager.service                                                                    loaded active running Network Manager
    polkit.service                                                                            loaded active running Authorization Manager
    postgresql.service                                                                        loaded active exited  PostgreSQL RDBMS
    postgresql@13-main.service                                                                loaded active running PostgreSQL Cluster 13-main
    rsyslog.service                                                                           loaded active running System Logging Service
    rtkit-daemon.service                                                                      loaded active running RealtimeKit Scheduling Policy Service
    ssh.service                                                                               loaded active running OpenBSD Secure Shell server
    sysstat.service

systemctl list-units --type mount
===================================

::

    systemctl list-units --type mount

::

    UNIT                          LOAD   ACTIVE SUB     DESCRIPTION
    -.mount                       loaded active mounted /
    boot-efi.mount                loaded active mounted /boot/efi
    boot.mount                    loaded active mounted /boot
    dev-hugepages.mount           loaded active mounted Huge Pages File System
    dev-mqueue.mount              loaded active mounted POSIX Message Queue File System
    proc-sys-fs-binfmt_misc.mount loaded active mounted Arbitrary Executable File Formats File System
    run-user-1001-gvfs.mount      loaded active mounted /run/user/1001/gvfs
    run-user-1001.mount           loaded active mounted /run/user/1001
    sys-fs-fuse-connections.mount loaded active mounted FUSE Control File System
    sys-kernel-debug.mount        loaded active mounted Kernel Debug File System

    LOAD   = Reflects whether the unit definition was properly loaded.
    ACTIVE = The high-level unit activation state, i.e. generalization of SUB.
    SUB    = The low-level unit activation state, values depend on unit type.

    10 loaded units listed. Pass --all to see loaded but inactive units, too.
    To show all installed unit files use 'systemctl list-unit-files'.


systemctl status postgresql
===========================

::

    systemctl status postgresql

::

    ● postgresql.service - PostgreSQL RDBMS
       Loaded: loaded (/lib/systemd/system/postgresql.service; enabled; vendor preset: enabled)
       Active: active (exited) since Wed 2021-04-28 05:53:43 CEST; 39min ago
      Process: 1209 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
     Main PID: 1209 (code=exited, status=0/SUCCESS)

    avril 28 05:53:43 uc045 systemd[1]: Starting PostgreSQL RDBMS...
    avril 28 05:53:43 uc045 systemd[1]: Started PostgreSQL RDBMS.

systemctl cat postgresql
============================

::

    systemctl cat postgresql

::

    # /lib/systemd/system/postgresql.service
    # systemd service for managing all PostgreSQL clusters on the system. This
    # service is actually a systemd target, but we are using a service since
    # targets cannot be reloaded.

    [Unit]
    Description=PostgreSQL RDBMS

    [Service]
    Type=oneshot
    ExecStart=/bin/true
    ExecReload=/bin/true
    RemainAfterExit=on

    [Install]
    WantedBy=multi-user.target

sudo systemctl disable mariadb.service
==========================================

::

    sudo systemctl disable mariadb.service

::

    Removed /etc/systemd/system/mysqld.service.
    Removed /etc/systemd/system/multi-user.target.wants/mariadb.service.
    Removed /etc/systemd/system/mysql.service.


systemctl cat fstrim.timer
===============================

::

    systemctl cat fstrim.timer

::

    # /lib/systemd/system/fstrim.timer
    [Unit]
    Description=Discard unused blocks once a week
    Documentation=man:fstrim

    [Timer]
    OnCalendar=weekly
    AccuracySec=1h
    Persistent=true

    [Install]
    WantedBy=timers.target


sudo systemctl edit --full fstrim.timer
=============================================

::

    sudo systemctl edit --full fstrim.timer


sudo systemctl restart postgresql
====================================

::

    sudo systemctl restart postgresql

systemctl list-dependencies postgresql
=========================================

::

    systemctl list-dependencies postgresql

::

    postgresql.service
    ● ├─postgresql@13-main.service
    ● ├─system.slice
    ● └─sysinit.target
    ●   ├─apparmor.service
    ●   ├─blk-availability.service
    ●   ├─dev-hugepages.mount
    ●   ├─dev-mqueue.mount
    ●   ├─keyboard-setup.service
    ●   ├─kmod-static-nodes.service
    ●   ├─lvm2-lvmpolld.socket
    ●   ├─lvm2-monitor.service
    ●   ├─plymouth-read-write.service
    ●   ├─plymouth-start.service
    ●   ├─proc-sys-fs-binfmt_misc.automount
    ●   ├─sys-fs-fuse-connections.mount
    ●   ├─sys-kernel-config.mount
    ●   ├─sys-kernel-debug.mount
    ●   ├─systemd-ask-password-console.path
    ●   ├─systemd-binfmt.service
    ●   ├─systemd-hwdb-update.service
    ●   ├─systemd-journal-flush.service
    ●   ├─systemd-journald.service
    ●   ├─systemd-machine-id-commit.service
    ●   ├─systemd-modules-load.service
    ●   ├─systemd-random-seed.service
    ●   ├─systemd-sysctl.service
    ●   ├─systemd-sysusers.service
    ●   ├─systemd-timesyncd.service
    ●   ├─systemd-tmpfiles-setup-dev.service
    ●   ├─systemd-tmpfiles-setup.service
    ●   ├─systemd-udev-trigger.service
    ●   ├─systemd-udevd.service
    ●   ├─systemd-update-utmp.service
    ●   ├─cryptsetup.target
    ●   ├─local-fs.target
    ●   │ ├─-.mount
    ●   │ ├─boot-efi.mount
    ●   │ ├─boot.mount
    ●   │ ├─systemd-fsck-root.service
    ●   │ └─systemd-remount-fs.service
    ●   └─swap.target
    ●     └─dev-mapper-uc045\x2d\x2dvg\x2dswap_1.swap

systemctl list-dependencies timers.target
=================================================

::

    sudo systemctl list-dependencies timers.target

::

    timers.target
    ● ├─anacron.timer
    ● ├─apt-daily-upgrade.timer
    ● ├─apt-daily.timer
    ● ├─logrotate.timer
    ● ├─man-db.timer
    ● └─systemd-tmpfiles-clean.timer

