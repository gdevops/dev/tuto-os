

.. _journalctl:

==========================================
journalctl
==========================================


sudo journalctl
===================

::

    sudo journalctl

::

    -- Logs begin at Wed 2021-04-28 05:53:39 CEST, end at Wed 2021-04-28 06:16:45 CEST. --
    avril 28 05:53:39 uc045 kernel: Linux version 4.19.0-16-amd64 (debian-kernel@lists.debian.org) (gcc version 8.3.0 (Debian 8.3.0-6)) #1 SMP Debian 4.19.181-1 (2021-03-19)
    avril 28 05:53:39 uc045 kernel: Command line: BOOT_IMAGE=/vmlinuz-4.19.0-16-amd64 root=/dev/mapper/uc045--vg-root ro quiet
    avril 28 05:53:39 uc045 kernel: x86/fpu: Supporting XSAVE feature 0x001: 'x87 floating point registers'
    avril 28 05:53:39 uc045 kernel: x86/fpu: Supporting XSAVE feature 0x002: 'SSE registers'
    avril 28 05:53:39 uc045 kernel: x86/fpu: Supporting XSAVE feature 0x004: 'AVX registers'
    avril 28 05:53:39 uc045 kernel: x86/fpu: Supporting XSAVE feature 0x008: 'MPX bounds registers'
    avril 28 05:53:39 uc045 kernel: x86/fpu: Supporting XSAVE feature 0x010: 'MPX CSR'


    avril 28 06:16:45 uc045 sudo[10897]: pam_unix(sudo:session): session opened for user root by (uid=0)
    avril 28 06:17:01 uc045 CRON[10901]: pam_unix(cron:session): session opened for user root by (uid=0)
    avril 28 06:17:01 uc045 CRON[10902]: (root) CMD (   cd / && run-parts --report /etc/cron.hourly)
    avril 28 06:17:01 uc045 CRON[10901]: pam_unix(cron:session): session closed for user root
    avril 28 06:17:15 uc045 dbus-daemon[652]: [system] Activating via systemd: service name='org.freedesktop.hostname1' unit='dbus-org.freedesktop.hostname1.service' requested by ':1.271' (uid=1001 pid=3886 comm="ge
    avril 28 06:17:15 uc045 systemd[1]: Starting Hostname Service...
    avril 28 06:17:15 uc045 dbus-daemon[652]: [system] Successfully activated service 'org.freedesktop.hostname1'
    avril 28 06:17:15 uc045 systemd[1]: Started Hostname Service.
    avril 28 06:17:45 uc045 systemd[1]: systemd-hostnamed.service: Succeeded.

sudo journalctl -u systemd-fsckd.service
=========================================

::

    sudo journalctl -u systemd-fsckd.service

::

    -- Logs begin at Wed 2021-04-28 05:53:39 CEST, end at Wed 2021-04-28 06:20:52 CEST. --
    avril 28 05:53:40 uc045 systemd[1]: Started File System Check Daemon to report status.
    avril 28 05:54:10 uc045 systemd[1]: systemd-fsckd.service: Succeeded.

sudo journalctl -u postgresql.service
=====================================

::

    sudo journalctl -u postgresql.service


::

    -- Logs begin at Wed 2021-04-28 05:53:39 CEST, end at Wed 2021-04-28 06:55:08 CEST. --
    avril 28 05:53:43 uc045 systemd[1]: Starting PostgreSQL RDBMS...
    avril 28 05:53:43 uc045 systemd[1]: Started PostgreSQL RDBMS.
    avril 28 06:43:33 uc045 systemd[1]: postgresql.service: Succeeded.
    avril 28 06:43:33 uc045 systemd[1]: Stopped PostgreSQL RDBMS.
    avril 28 06:43:33 uc045 systemd[1]: Stopping PostgreSQL RDBMS...
    avril 28 06:43:35 uc045 systemd[1]: Starting PostgreSQL RDBMS...
    avril 28 06:43:35 uc045 systemd[1]: Started PostgreSQL RDBMS.
    avril 28 06:43:41 uc045 systemd[1]: postgresql.service: Succeeded.
    avril 28 06:43:41 uc045 systemd[1]: Stopped PostgreSQL RDBMS.
    avril 28 06:43:41 uc045 systemd[1]: Stopping PostgreSQL RDBMS...
    avril 28 06:43:43 uc045 systemd[1]: Starting PostgreSQL RDBMS...
    avril 28 06:43:43 uc045 systemd[1]: Started PostgreSQL RDBMS.




sudo journalctl -r -p err --since="today"
============================================

::

    sudo journalctl -r -p err --since="today"

::

    -- Logs begin at Wed 2021-04-28 05:53:39 CEST, end at Wed 2021-04-28 06:24:05 CEST. --
    avril 28 05:53:45 uc045 kernel: ucsi_acpi USBC000:00: PPM init failed (-110)
    avril 28 05:53:40 uc045 kernel: Bluetooth: hci0: Failed to load Intel firmware file (-2)
    avril 28 05:53:40 uc045 kernel: bluetooth hci0: firmware: failed to load intel/ibt-20-1-3.sfi (-2)
    avril 28 05:53:40 uc045 kernel: iTCO_wdt iTCO_wdt: can't request region for resource [mem 0x00c5fffc-0x00c5ffff]
    avril 28 05:53:39 uc045 kernel: [drm:intel_modeset_init [i915]] *ERROR* LSPCON init failed on port D
    avril 28 05:53:39 uc045 kernel: [drm:lspcon_init [i915]] *ERROR* Failed to probe lspcon
    avril 28 05:53:39 uc045 kernel: firmware_class: See https://wiki.debian.org/Firmware for information about missing firmware
    avril 28 05:53:39 uc045 kernel: i915 0000:00:02.0: firmware: failed to load i915/kbl_dmc_ver1_04.bin (-2)
    avril 28 05:53:39 uc045 kernel: ACPI Error: Method parse/execution failed \_SB._OSC, AE_AML_BUFFER_LIMIT (20180810/psparse-516)
    avril 28 05:53:39 uc045 kernel: ACPI Error: Field [CAP1] at bit offset/length 64/32 exceeds size of target Buffer (64 bits) (20180810/dsopcode-201)


