

.. index::
   pair: Linux; Kernel


.. _linux_kernel:

==========================================
GNU/Linux kernel
==========================================

.. seealso::

   - https://en.wikipedia.org/wiki/Linux_kernel
   - https://github.com/torvalds/linux
   - https://kernelnewbies.org


.. figure:: Tux.svg.png
   :align: center

.. toctree::
   :maxdepth: 3

   definition/definition
   commands/commands
   versions/versions
