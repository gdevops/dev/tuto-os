.. index::
   ! Bookworm

.. _bookworkm_2023_05_23:

===========================================================================
2023-05-23 **Debian : Mettre à niveau de bullseye 11 vers bookworm 12**
===========================================================================

- https://www.linuxtricks.fr/wiki/debian-mettre-a-niveau-de-bullseye-11-vers-bookworm-12
- https://fr.wikipedia.org/wiki/Debian#Chronologie_de_Debian_GNU/Linux

Introduction
=================

Dans cet article, nous allons voir comment mettre à niveau une Debian 11
vers Debian 12.

Dans le cas d'un serveur, je vous conseille une réinstallation du système
et de procéder à une migration des données.

Dans le cas d'une station de travail (ou ordinateur portable), procédez
à une sauvegarde de vos données avant la migration.

Prérequis
===========

Dans un premier temps, mettre à jour complètement son système :

::

    apt update
    apt full-upgrade

Modification des sources

Debian 11 a comme nom de code "bullseye".
Debian 12 a comme nom de code "bookworm"

On remplacera donc les occurrences bullseye en bookworm dans le sources.list via sed :

Copier vers le presse-papierCode BASH::

    sed -i 's/bullseye/bookworm/g' /etc/apt/sources.list


Si vous voulez le faire avec vi/vim ou nano vous pouvez aussi :)

Si vous aviez activé les dépôts "non-free", songez à rajouter "non-free-firmware"
qui est un composant supplémentaire sur cette version.

Je vous propose cette commande sed::

    sed -i 's/non-free/non-free non-free-firmware/g' /etc/apt/sources.list

Procéder à la mise à niveau

Maintenant, on rafraîchit les dépôts :

Copier vers le presse-papierCode BASH :

apt update



Et on met à jour le système::


    apt full-upgrade



Après le téléchargement, et avant l'installation, une page indique les
principales nouveautés concernant les paquets installés sur votre système
qui seront mis à jour.

Pensez à les lire avec attention.

Défilez avec espace, et quittez avec q.

Pendant l'installation, certaines fenêtres pourront s'afficher vous demandant des actions :

::

    Package configuration
     ┌────────────────┤ Configuring libc6:amd64 ├────────────────┐
     │                                                           │
     │ Restart services during package upgrades without asking?  │
     │                                                           │
     │               <Yes>                  <No>                 │
     │                                                           │
     └───────────────────────────────────────────────────────────┘


Validez le choix Yes pour que la mise à niveau se déroule.

Lorsque certains fichiers de configuration peuvent avoir une mise à jour, un message de ce type apparait (comme ici pour crontab sur mon système) :

::

    Configuration file '/etc/crontab'
     ==> Modified (by you or by a script) since installation.
     ==> Package distributor has shipped an updated version.
       What would you like to do about it ?  Your options are:
        Y or I  : install the package maintainer's version
        N or O  : keep your currently-installed version
          D     : show the differences between the versions
          Z     : start a shell to examine the situation
     The default action is to keep your current version.
    *** crontab (Y/I/N/O/D/Z) [default=N] ?



Répondez en fonction de ce qui vous correspond. Si vous ne savez pas,
laissez le choix par défaut.

Une fois terminé, redémarrez::

    reboot



Vérifications
==================

Vérifiez que vous êtes bien sur la nouvelle version::

    cat /etc/debian_version

Pensez à vérifier le bon fonctionnement de vos applicatifs !
