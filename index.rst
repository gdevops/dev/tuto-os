
.. figure:: images/debian_os.ico
   :align: center

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/dev/tuto-os/rss.xml>`_

.. _os_tuto:

================================
**Operating System tutorial**
================================

.. toctree::
   :maxdepth: 3

   definition/definition
   gnu_linux/gnu_linux
   linux_kernel/linux_kernel
   news/news
