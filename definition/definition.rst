
.. index::
   pair: OS; Definition

.. _os_definition:

==============================
OS definition
==============================

.. contents::
   :depth: 3


Wikipedia definition
======================

.. seealso::

   - https://en.wikipedia.org/wiki/Operating_system

An operating system (OS) is system software that manages computer hardware and
software resources and provides common services for computer programs.

Time-sharing operating systems schedule tasks for efficient use of the system
and may also include accounting software for cost allocation of processor time,
mass storage, printing, and other resources.
