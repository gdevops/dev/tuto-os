

.. index::
   pair: GNU/Linux ; OS


.. _gnulinux_os:

=============================
GNU/Linux Operating Systems
=============================


.. toctree::
   :maxdepth: 6

   debian_based/debian_based
   nixos/nixos
   rpm_based/rpm_based
