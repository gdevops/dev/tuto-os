
.. _nixos_def:

==========================================
NixOS definition
==========================================

.. seealso::

   - https://nixos.org/nixos/about.html

.. contents::
   :depth: 3
   
Definition
============
   
NixOS is a GNU/Linux distribution that aims to improve the state of the 
art in system configuration management. 

In existing distributions, actions such as upgrades are dangerous: 
upgrading a package can cause other packages to break, upgrading an 
entire system is much less reliable than reinstalling from scratch, 
you can’t safely test what the results of a configuration change 
will be, you cannot easily undo changes to the system, and so on. 

We want to change that....

Declarative
============

NixOS has a completely declarative approach to configuration management
you write a specification of the desired configuration of your system 
in NixOS’s modular language, and NixOS takes care of making it happen.

Reliable
==========

NixOS has atomic upgrades and rollbacks. It’s always safe to try an 
upgrade or configuration change: if things go wrong, you can always 
roll back to the previous configuration.

DevOps-friendly
==================

Declarative specs and safe upgrades make NixOS a great system for 
DevOps use. 

NixOps, the NixOS cloud deployment tool, allows you to provision and 
manage networks of NixOS machines in environments like Amazon EC2 
and VirtualBox.

