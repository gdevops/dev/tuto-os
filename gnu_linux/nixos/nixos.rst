
.. index::
   pair: Nix ; OS

.. _nixos:

==========================================
NixOS
==========================================

.. seealso::

   - https://nixos.org/nixos/


.. toctree::
   :maxdepth: 3

   definition/definition
