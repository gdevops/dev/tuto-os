
.. _debian_11_to_12:

==========================================
**Passage de Debian 11 à Debian 12**
==========================================

- https://www.debian.org/releases/bookworm/amd64/release-notes/ch-upgrading.fr.html#minimal-upgrade


Processus de mise à jour
============================

- https://www.nextinpact.com/article/71867/debian-12-comment-mettre-a-jour-vers-nouvelle-version-majeure

::

    - apt upgrade --without-new-pkgs


Installer toutes les mises à jour en attente
=================================================

Avant de s’attaquer à Debian 12, il faut vérifier qu’il n’y a pas d’autres
mises à jour en attente pour Debian 11.

Exécutez les deux commandes suivantes dans un terminal::

    sudo apt update
    sudo apt upgrade

la première vérifie les dépôts et met à jour la liste des nouveaux paquets
en attente. La seconde récupère les mises à jour potentielles et les installe.


Mettre à jour le fichier des dépôts

L’étape la plus importante est la modification du fichier sources.list
situé dans /etc/apt/.

Premièrement, il faut s’assurer que vous avez bien un compte avec des
droits administrateur. Si besoin, créez-le dans la section Utilisateurs
dans le panneau des Paramètres.

Deuxièmement, un changement important est intervenu dans Debian 12 :
l’apparition du dépôt non-free-firmware.
Il est spécifiquement créé pour les firmwares non-libres, qui ne sont
donc plus mélangés avec les autres paquets propriétaires du dépôt non-free.

En ajoutant ce dépôt au fichier, l’installeur de Debian va pouvoir les récupérer automatiquement.

Il existe deux manières d’accéder au fichier sources.list : soit par le
gestionnaire de fichiers puis en l’ouvrant avec l’éditeur par défaut,
soit en restant dans le terminal et en utilisant nano. Là encore,
choisissez l’option que vous préférez, le résultat est le même.

Dans les deux cas, il faudra réaliser deux séries d’opérations : mettre
en commentaire les lignes faisant référence à l’ancienne version de Debian
(bullseye) et ajouter celles dédiées à la nouvelle (bookworm).

Lignes à ajouter au fichier /etc/apt/sources.list
----------------------------------------------------

::

    deb http://deb.debian.org/debian/ bookworm main
    deb-src http://deb.debian.org/debian/ bookworm main

    deb http://security.debian.org/debian-security bookworm-security main
    deb-src http://security.debian.org/debian-security bookworm-security main

    deb http://deb.debian.org/debian/ bookworm-updates main
    deb-src http://deb.debian.org/debian/ bookworm-updates main

    deb http://deb.debian.org/debian bookworm non-free non-free-firmware
    deb-src http://deb.debian.org/debian bookworm non-free non-free-firmware

    deb http://deb.debian.org/debian-security bookworm-security non-free non-free-firmware
    deb-src http://deb.debian.org/debian-security bookworm-security non-free non-free-firmware

    deb http://deb.debian.org/debian bookworm-updates non-free non-free-firmware
    deb-src http://deb.debian.org/debian bookworm-updates non-free non-free-firmware


Lignes commentées (bullseye) dans /etc/apt/sources.list
-------------------------------------------------------------

::

    #
    # deb cdrom:[Debian GNU/Linux 10.3.0 _Buster_ - Official amd64 NETINST 20200208-12:07]/ bullseye main

    # deb http://deb.debian.org/debian/ bullseye main
    # deb-src http://deb.debian.org/debian/ bullseye main

    # deb https://deb.debian.org/debian-security bullseye-security main contrib
    # deb-src https://deb.debian.org/debian-security bullseye-security main contrib

    # bullseye-updates, previously known as 'volatile'
    # deb http://deb.debian.org/debian/ bullseye-updates main
    # deb-src http://deb.debian.org/debian/ bullseye-updates main

    # This system was installed using small removable media
    # (e.g. netinst, live or single CD). The matching "deb cdrom"
    # entries were disabled at the end of the installation process.
    # For information about how to configure apt package sources,
    # deb [arch=amd64] https://download.docker.com/linux/debian bullseye stable


Lancer le processus d’installation
============================================

Une fois le fichier sources.list modifié, et sans redémarrer la machine,
entrez à nouveau la commande::

    sudo apt update


::

    ✦ ❯ sudo apt update
    Atteint :1 http://deb.debian.org/debian bookworm InRelease
    Atteint :2 http://security.debian.org/debian-security bookworm-security InRelease
    Atteint :3 http://deb.debian.org/debian bookworm-updates InRelease
    Atteint :4 http://deb.debian.org/debian-security bookworm-security InRelease
    Atteint :5 http://packages.microsoft.com/repos/code stable InRelease
    Atteint :6 http://apt.dalibo.org/labs bookworm-dalibo InRelease
    Atteint :7 https://apt.grafana.com stable InRelease
    Atteint :8 https://updates.signal.org/desktop/apt xenial InRelease
    Ign :9 https://repo.vivaldi.com/stable/deb stable InRelease
    Atteint :10 https://repo.vivaldi.com/stable/deb stable Release
    Réception de :11 https://repo.charm.sh/apt * InRelease
    Atteint :12 https://apt.postgresql.org/pub/repos/apt bookworm-pgdg InRelease
    6 615 o réceptionnés en 1s (5 448 o/s)
    Lecture des listes de paquets... Fait
    Construction de l'arbre des dépendances... Fait
    Lecture des informations d'état... Fait
    2470 paquets peuvent être mis à jour. Exécutez « apt list --upgradable » pour les voir.


Cette fois, le processus va prendre plus de temps, la liste des modifications
étant beaucoup plus importante.

Pour lancer l’installation de Debian 12 (Bookworm), nous vous proposons
une démarche en deux temps.


sudo apt upgrade --without-new-pkgs
----------------------------------------

D'abord, une commande qui va mettre à jour les paquets installés, sans
installer ou supprimer d'autres paquets::

    sudo apt upgrade --without-new-pkgs


::

    ✦ ❯ sudo apt upgrade --without-new-pkgs
    Lecture des listes de paquets... Fait
    Construction de l'arbre des dépendances... Fait
    Lecture des informations d'état... Fait
    Calcul de la mise à jour... Fait
    Les paquets suivants ont été installés automatiquement et ne sont plus nécessaires :
      crda gedit-plugin-commander gedit-plugin-find-in-files gedit-plugin-translate gist ruby-json vlc-l10n vlc-plugin-notify vlc-plugin-samba vlc-plugin-video-splitter vlc-plugin-visualization x11proto-input-dev
      x11proto-randr-dev x11proto-xinerama-dev
    Veuillez utiliser « sudo apt autoremove » pour les supprimer.
    Les paquets suivants ont été conservés :
      accountsservice acl acpi aisleriot alsa-utils anacron apache2 apache2-bin apache2-data apache2-utils apparmor appstream apt apt-utils aspell asymptote at at-spi2-core avahi-daemon base-passwd bash biber
      bind9-dnsutils bind9-host bind9-libs binfmt-support binutils binutils-common binutils-x86-64-linux-gnu blueman bluez bluez-obexd bolt brasero brasero-cdrkit brasero-common bsdextrautils bsdutils bubblewrap
      busybox bzip2 ccze cdrdao cheese chktex chromium chromium-common chromium-l10n chromium-sandbox cifs-utils cinnamon cinnamon-common cinnamon-control-center cinnamon-control-center-data
      cinnamon-control-center-goa cinnamon-core cinnamon-desktop-data cinnamon-desktop-environment cinnamon-l10n cinnamon-screensaver cinnamon-session cinnamon-session-common cinnamon-settings-daemon cjs cmake
      cmake-data coinor-libcbc3 coinor-libcgl1 coinor-libclp1 colord coreutils cpio cpp cracklib-runtime cron cryptsetup cryptsetup-bin cryptsetup-initramfs cups cups-browsed cups-bsd cups-client cups-core-drivers
      cups-daemon cups-filters cups-filters-core-drivers cups-ipp-utils cups-ppdc curl dash dbus dbus-user-session dbus-x11 dconf-cli dconf-gsettings-backend dconf-service debianutils deja-dup devscripts dfc dia
      dia-common diffstat diffutils direnv dirmngr discover dmeventd dmidecode dmsetup dnsmasq-base doxygen dpkg dpkg-dev dracut-core duplicity dvdauthor dvisvgm dwz e2fsprogs efibootmgr eject enchant-2 eog
      espeak-ng-data evince evince-common exfat-fuse exif exim4-base exim4-daemon-light fakeroot falkon fdisk ffmpeg fig2dev file file-roller findutils firefox-esr firefox-esr-l10n-fr flameshot fontconfig
      fonts-texgyre fortune-mod fuse3 fwupd g++ galera-4 gawk gcc gconf-service gconf2 gconf2-common gcr gdb gdisk geany geany-common gedit gedit-common gedit-plugin-bookmarks gedit-plugin-bracket-completion
      gedit-plugin-character-map gedit-plugin-code-comment gedit-plugin-color-picker gedit-plugin-color-schemer gedit-plugin-draw-spaces gedit-plugin-git gedit-plugin-join-lines gedit-plugin-multi-edit
      gedit-plugin-session-saver gedit-plugin-smart-spaces gedit-plugin-synctex gedit-plugin-terminal gedit-plugin-text-size gedit-plugin-word-completion genisoimage geoclue-2.0 gettext gettext-base gh ghostscript
      gimp gimp-data gir1.2-accountsservice-1.0 gir1.2-atk-1.0 gir1.2-atspi-2.0 gir1.2-cinnamondesktop-3.0 gir1.2-cmenu-3.0 gir1.2-cogl-1.0 gir1.2-coglpango-1.0 gir1.2-cvc-1.0 gir1.2-freedesktop
      gir1.2-gdkpixbuf-2.0 gir1.2-ggit-1.0 gir1.2-glib-2.0 gir1.2-gnomedesktop-3.0 gir1.2-granite-1.0 gir1.2-gst-plugins-bad-1.0 gir1.2-gst-plugins-base-1.0 gir1.2-gstreamer-1.0 gir1.2-gtk-3.0 gir1.2-harfbuzz-0.0
      gir1.2-ibus-1.0 gir1.2-javascriptcoregtk-4.0 gir1.2-meta-muffin-0.0 gir1.2-nemo-3.0 gir1.2-nm-1.0 gir1.2-notify-0.7 gir1.2-pango-1.0 gir1.2-polkit-1.0 gir1.2-rb-3.0 gir1.2-soup-2.4 gir1.2-totem-1.0
      gir1.2-upowerglib-1.0 gir1.2-vte-2.91 gir1.2-webkit2-4.0 gir1.2-xapp-1.0 git git-man git-svn gjs gkbd-capplet glances glib-networking glib-networking-services gnome-2048 gnome-calculator gnome-chess
      gnome-control-center gnome-control-center-data gnome-desktop3-data gnome-disk-utility gnome-font-viewer gnome-keyring gnome-nibbles gnome-online-accounts gnome-paint gnome-settings-daemon
      gnome-settings-daemon-common gnome-software gnome-software-common gnome-sound-recorder gnome-sudoku gnome-system-monitor gnome-taquin gnome-terminal gnome-terminal-data gnome-user-share gnote gnupg
      gnupg-l10n gnupg-utils gnupg2 gpg gpg-agent gpg-wks-client gpg-wks-server gpgconf gpgsm gpgv graphviz grep grilo-plugins-0.3 grisbi grisbi-common groff-base grub-common grub-efi-amd64 grub-efi-amd64-bin
      grub-efi-amd64-signed grub-pc-bin grub2-common gsasl-common gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-libav gstreamer1.0-plugins-bad gstreamer1.0-plugins-base gstreamer1.0-plugins-good
      gstreamer1.0-plugins-ugly gstreamer1.0-pulseaudio gstreamer1.0-x gtk-update-icon-cache guile-2.2-libs gvfs gvfs-backends gvfs-common gvfs-daemons gvfs-fuse gvfs-libs gzip hdparm hexchat hexchat-common
      hexchat-perl hexchat-plugins hexchat-python3 hoichess hostname hplip hplip-data htop ibus ibus-data ibus-gtk ibus-gtk3 icu-devtools ifupdown imagemagick-6.q16 info init-system-helpers initramfs-tools
      initramfs-tools-core inkscape install-info intel-media-va-driver ipp-usb iproute2 iptables iputils-ping isc-dhcp-client ispell jmtpfs kaccounts-providers kactivities-bin kactivitymanagerd kbd kdeconnect
      kded5 keditbookmarks keyutils kinit kio kmod kpackagelauncherqml kpackagetool5 kpartx krita krita-gmic ksnip kwayland-data kwayland-integration latex-cjk-all latex-cjk-chinese latex-cjk-common
      latex-cjk-japanese latex-cjk-korean latex-cjk-thai lcdf-typetools ldap-utils less libaacs0 libaccountsservice0 libacl1 libalgorithm-diff-xs-perl libann0 libapparmor1 libappstream4 libapr1 libaprutil1
      libaprutil1-dbd-sqlite3 libaprutil1-ldap libapt-pkg-perl libapt-pkg6.0 libarchive-dev libarchive13 libargon2-1 libasan6 libasound2 libasound2-data libasound2-plugins libaspell15 libassuan0 libasyncns0
      libatk1.0-0 libatk1.0-dev libatkmm-1.6-1v5 libatomic1 libatopology2 libatspi2.0-0 libatspi2.0-dev libaudit1 libautovivification-perl libavahi-client3 libavahi-common-data libavahi-common3 libavahi-core7
      libavahi-glib1 libavahi-gobject0 libavahi-ui-gtk3-0 libayatana-appindicator3-1 libayatana-ido3-0.4-0 libayatana-indicator3-7 libb-hooks-op-check-perl libbabeltrace1 libbabl-0.1-0 libbdplus0 libbinutils
      libblkid-dev libblkid1 libblockdev-crypto2 libblockdev-utils2 libblockdev2 libbluray2 libboost-filesystem1.74.0 libboost-iostreams1.74.0 libboost-locale1.74.0 libboost-regex1.74.0 libboost-thread1.74.0
      libbrasero-media3-1 libbrlapi0.8 libbsd0 libbz2-1.0 libbz2-dev libc++1 libc-bin libc-dev-bin libc-devtools libc6 libc6-dbg libc6-dev libcairo-gobject-perl libcairo-gobject2 libcairo-perl
      libcairo-script-interpreter2 libcairo2 libcairo2-dev libcanberra-pulse libcanberra0 libcap-dev libcap-ng0 libcap2 libcap2-bin libcc1-0 libcdio-cdda2 libcdio19 libcdr-0.1-1 libcheese-gtk25 libcheese8
      libchromaprint1 libcinnamon-control-center1 libcinnamon-desktop4 libcinnamon-menu-3-0 libcjs0 libclass-c3-xs-perl libclass-xsaccessor-perl libclone-perl libclucene-core1v5 libcogl-pango20 libcogl-path20
      libcogl20 libcoin80c libcollada-dom2.5-dp0 libcommon-sense-perl libcpanel-json-xs-perl libcrypt-dev libcrypt-ssleay-perl libcrypt1 libcryptsetup12 libcscreensaver0 libctf0 libcups2 libcupsfilters1
      libcupsimage2 libcurl3-gnutls libcurl4 libcvc0 libdap27 libdapclient6v5 libdata-messagepack-perl libdate-simple-perl libdatetime-perl libdb5.3 libdbd-mysql-perl libdbi-perl libdbus-1-3 libdbus-1-dev
      libdconf1 libde265-0 libdebuginfod1 libdevel-callchecker-perl libdevel-caller-perl libdevel-lexalias-perl libdevel-size-perl libdevmapper-event1.02.1 libdevmapper1.02.1 libdiscover2 libdjvulibre21
      libdmraid1.0.0.rc16 libdpkg-perl libdrm-amdgpu1 libdrm-intel1 libdrm-nouveau2 libdrm-radeon1 libdrm2 libdvdread8 libdw1 libe-book-0.1-1 libedit2 libegl-dev libegl-mesa0 libegl1 libelf1
      libemail-address-xs-perl libenchant-2-2 libencode-eucjpms-perl libencode-hanextra-perl libencode-jis2k-perl libespeak-ng1 libetonyek-0.1-1 libevdev2 libevdocument3-4 libevent-2.1-7 libevview3-3 libexempi8
      libexiv2-27 libext2fs2 libfakeroot libfcgi-perl libfdisk1 libffi-dev libfftw3-double3 libfftw3-single3 libfido2-1 libfile-fcntllock-perl libflashrom1 libflatpak0 libflite1 libfontconfig-dev libfontconfig1
      libfontconfig1-dev libfreerdp2-2 libfreetype-dev libfreetype6 libfreetype6-dev libfstrm0 libfuse2 libfuse3-3 libfyba0 libgail-3-0 libgbm1 libgc1 libgcc-s1 libgck-1-0 libgconf-2-4 libgcr-base-3-1
      libgcr-ui-3-1 libgcrypt20 libgd3 libgdbm-compat4 libgdbm6 libgdcm3.0 libgdk-pixbuf-2.0-0 libgdk-pixbuf-2.0-dev libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common libgegl-0.4-0 libgeoip1 libgeos-c1v5 libgeotiff5
      libges-1.0-0 libgexiv2-2 libgfortran5 libgimp2.0 libgirepository-1.0-1 libgit2-glib-1.0-0 libgjs0g libgl-dev libgl1 libgl1-mesa-dri libgladeui-2-13 libglapi-mesa libgles-dev libgles1 libgles2
      libglib-object-introspection-perl libglib-perl libglib2.0-0 libglib2.0-bin libglib2.0-dev libglib2.0-dev-bin libglibmm-2.4-1v5 libglvnd-dev libglvnd0 libglx-dev libglx-mesa0 libglx0 libgme0 libgmic1
      libgnome-bluetooth13 libgnutls-dane0 libgnutls-openssl27 libgnutls28-dev libgnutls30 libgoa-1.0-0b libgoa-backend-1.0-1 libgoffice-0.10-10 libgomp1 libgpg-error0 libgpgme11 libgpgmepp6 libgphoto2-6
      libgphoto2-port12 libgpm2 libgpod-common libgpod4 libgranite-common libgranite-dev libgraphicsmagick-q16-3 libgrilo-0.3-0 libgs9-common libgsf-1-114 libgslcblas0 libgspell-1-2 libgssapi-krb5-2
      libgstreamer-gl1.0-0 libgstreamer-plugins-bad1.0-0 libgstreamer-plugins-base1.0-0 libgstreamer1.0-0 libgtk-3-0 libgtk-3-bin libgtk-3-common libgtk-3-dev libgtk-vnc-2.0-0 libgtkmm-3.0-1v5 libgts-0.7-5
      libgts-bin libgupnp-igd-1.0-4 libgutenprint-common libgutenprint9 libgvc6 libgvnc-1.0-0 libgvpr2 libgxps2 libharfbuzz-dev libharfbuzz-gobject0 libharfbuzz-icu0 libharfbuzz0b libhash-fieldhash-perl
      libhdf4-0-alt libhdf5-103-1 libheif1 libhfstospell11 libhpmud0 libhtml-parser-perl libhttp-message-perl libhunspell-1.7-0 libibus-1.0-5 libical3 libicu-dev libimage-magick-q16-perl libimobiledevice6
      libinput-bin libinput10 libio-pty-perl libip4tc2 libip6tc2 libipc-shareable-perl libipt2 libiptc0 libitm1 libjack-jackd2-0 libjavascriptcoregtk-4.0-18 libjson-c-dev libjson-c5 libjson-xs-perl libk5crypto3
      libkaccounts2 libkf5activities5 libkf5archive5 libkf5auth-data libkf5auth5 libkf5authcore5 libkf5bookmarks-data libkf5bookmarks5 libkf5completion-data libkf5completion5 libkf5config-bin libkf5config-data
      libkf5configcore5 libkf5configgui5 libkf5configwidgets-data libkf5configwidgets5 libkf5contacts-data libkf5contacts5 libkf5coreaddons-data libkf5coreaddons5 libkf5crash5 libkf5dbusaddons-bin
      libkf5dbusaddons-data libkf5dbusaddons5 libkf5declarative-data libkf5declarative5 libkf5doctools5 libkf5globalaccel-bin libkf5globalaccel-data libkf5globalaccel5 libkf5globalaccelprivate5 libkf5guiaddons5
      libkf5iconthemes-bin libkf5iconthemes-data libkf5iconthemes5 libkf5idletime5 libkf5jobwidgets-data libkf5jobwidgets5 libkf5js5 libkf5kcmutils-data libkf5kcmutils5 libkf5kexiv2-15.0.0 libkf5kiocore5
      libkf5kiofilewidgets5 libkf5kiogui5 libkf5kiowidgets5 libkf5kirigami2-5 libkf5notifications-data libkf5notifications5 libkf5package-data libkf5package5 libkf5parts-data libkf5parts-plugins libkf5parts5
      libkf5people-data libkf5people5 libkf5peoplebackend5 libkf5peoplewidgets5 libkf5plasma5 libkf5plasmaquick5 libkf5pty-data libkf5pty5 libkf5purpose-bin libkf5purpose5 libkf5quickaddons5 libkf5service-bin
      libkf5service-data libkf5service5 libkf5solid5 libkf5solid5-data libkf5sonnet5-data libkf5sonnetcore5 libkf5sonnetui5 libkf5textwidgets-data libkf5textwidgets5 libkf5wallet-bin libkf5wallet-data
      libkf5wallet5 libkf5waylandclient5 libkf5windowsystem-data libkf5windowsystem5 libkf5xmlgui-bin libkf5xmlgui-data libkf5xmlgui5 libkmlbase1 libkmldom1 libkmlengine1 libkmod2 libkpathsea6 libkrb5-3
      libkrb5support0 libksba8 libkwalletbackend5-5 liblangtag-common liblangtag1 liblcms2-utils libldap2-dev libldb2 liblept5 libleptonica-dev liblibreoffice-java liblilv-0-0 liblirc-client0 liblist-allutils-perl
      liblist-moreutils-xs-perl liblist-someutils-xs-perl liblocale-gettext-perl liblouis20 liblouisutdml-bin liblouisutdml9 liblsan0 libltdl-dev libltdl7 liblua5.2-0 liblua5.3-0 liblvm2cmd2.03 liblzma-dev
      liblzma5 libmagic-mgc libmagic1 libmagick++-6.q16-8 libmagickcore-6.q16-6 libmagickcore-6.q16-6-extra libmariadb-dev libmariadb-dev-compat libmariadb3 libmath-random-isaac-xs-perl libmaxminddb0 libmd0
      libmediaart-2.0-0 libmfx1 libmount-dev libmount1 libmouse-perl libmozjs-78-0 libmpeg2encpp-2.1-0 libmpg123-0 libmplex2-2.1-0 libmspub-0.1-1 libmtp-runtime libmtp9 libmuffin0 libmwaw-0.3-3 libncurses-dev
      libncurses5-dev libncurses6 libncursesw5-dev libncursesw6 libnemo-extension1 libneon27-gnutls libnet-cups-perl libnet-dbus-perl libnet-libidn-perl libnet-ssleay-perl libnewt0.52 libnftnl11 libnl-3-200
      libnl-genl-3-200 libnl-route-3-200 libnm0 libnma-common libnma0 libnotify4 libnspr4 libnss-mdns libnss-myhostname libnss-systemd libnss3 libnss3-tools libnuma1 libnumbertext-1.0-0 libodbc1 libogdi4.1
      libopengl-dev libopengl0 libopenjp2-7 libopenmpt0 libopenni2-0 libopenscenegraph161 libopenthreads21 libossp-uuid-perl libostree-1-1 libotr5 libp11-kit-dev libp11-kit0 libpackage-stash-xs-perl
      libpadwalker-perl libpam-gnome-keyring libpam-modules libpam-modules-bin libpam-systemd libpam0g libpango-1.0-0 libpango1.0-dev libpangocairo-1.0-0 libpangoft2-1.0-0 libpangomm-1.4-1v5 libpangoxft-1.0-0
      libpaper-utils libpaper1 libparams-classify-perl libparams-util-perl libparams-validate-perl libparted-fs-resize0 libparted2 libpcap0.8 libpci3 libpciaccess0 libpcre16-3 libpcre2-16-0 libpcre2-32-0
      libpcre2-8-0 libpcre2-dev libpcre3 libpcre3-dev libpcre32-3 libpcrecpp0v5 libpcsclite1 libpeas-1.0-0 libperlio-gzip-perl libperlio-utf8-strict-perl libpipeline1 libpipewire-0.3-0 libpipewire-0.3-modules
      libplist3 libplymouth5 libpng-dev libpng-tools libpng16-16 libpolkit-agent-1-0 libpolkit-gobject-1-0 libpoppler-cpp0v5 libpoppler-glib8 libpoppler-qt5-1 libpopt0 libpq-dev libpq5 libproc-processtable-perl
      libproxy-tools libproxy1v5 libpsl5 libpstoedit0c2a libptexenc1 libpulse-mainloop-glib0 libpulse0 libpulsedsp libpurple0 libpwquality1 libpython3-stdlib libqca-qt5-2 libqca-qt5-2-plugins libqt5core5a
      libqt5dbus5 libqt5designer5 libqt5gui5 libqt5multimedia5 libqt5multimedia5-plugins libqt5multimediagsttools5 libqt5multimediaquick5 libqt5multimediawidgets5 libqt5network5 libqt5opengl5 libqt5positioning5
      libqt5printsupport5 libqt5qml5 libqt5qmlmodels5 libqt5qmlworkerscript5 libqt5quick5 libqt5quickcontrols2-5 libqt5quickparticles5 libqt5quicktemplates2-5 libqt5quickwidgets5 libqt5sql5 libqt5sql5-sqlite
      libqt5svg5 libqt5test5 libqt5texttospeech5 libqt5waylandclient5 libqt5waylandcompositor5 libqt5webchannel5 libqt5webengine-data libqt5webengine5 libqt5webenginecore5 libqt5webenginewidgets5 libqt5widgets5
      libqt5x11extras5 libqt5xml5 libquadmath0 libqxp-0.0-0 librabbitmq4 libraptor2-0 librasqal3 libraw20 librdf0 libre-engine-re2-perl libre2-9 libreadline-dev libreadline8 libref-util-xs-perl
      libregexp-pattern-license-perl libreoffice-base-core libreoffice-calc libreoffice-common libreoffice-core libreoffice-draw libreoffice-gnome libreoffice-gtk3 libreoffice-help-common libreoffice-help-fr
      libreoffice-impress libreoffice-l10n-fr libreoffice-math libreoffice-writer librevenge-0.0-0 librhash0 librhythmbox-core10 librsvg2-2 librsvg2-bin librsvg2-common librsync2 librubberband2 libsane-common
      libsane-hpaio libsane1 libsasl2-2 libsasl2-dev libsasl2-modules libsdl-image1.2 libsdl1.2debian libsdl2-2.0-0 libsecret-1-0 libselinux1 libselinux1-dev libsemanage-common libsensors5 libsereal-decoder-perl
      libsereal-encoder-perl libserf-1-1 libshout3 libslang2 libsmartcols1 libsmbclient libsndfile1 libsndio7.0 libsnmp40 libsort-key-perl libsoup-gnome2.4-1 libsoup2.4-1 libsource-highlight4v5 libspa-0.2-modules
      libspandsp2 libspatialite7 libspectre1 libspeechd2 libsphinxbase3 libspice-client-glib-2.0-8 libspice-client-gtk-3.0-5 libsqlcipher0 libsqlite3-0 libsqlite3-dev libss2 libssh-gcrypt-4 libssh2-1 libssl-dev
      libstdc++6 libstring-copyright-perl libsub-identify-perl libsub-name-perl libsvn-perl libsvn1 libsynctex2 libsys-cpuaffinity-perl libsystemd0 libtalloc2 libtcl8.6 libtdb1 libteckit0 libterm-readkey-perl
      libtesseract-dev libtevent0 libtexlua53 libtexluajit2 libtext-bibtex-perl libtext-charwidth-perl libtext-csv-perl libtext-csv-xs-perl libtext-iconv-perl libtext-levenshteinxs-perl
      libtext-markdown-discount-perl libtext-xslate-perl libtime-moment-perl libtinfo6 libtirpc-dev libtirpc3 libtk8.6 libtotem-plparser18 libtotem0 libtsan0 libtss2-esys-3.0.2-0 libtype-tiny-perl
      libtype-tiny-xs-perl libubsan1 libudev1 libunbound8 libunicode-linebreak-perl libunicode-utf8-perl libunistring2 libuno-cppu3 libuno-cppuhelpergcc3-3 libuno-sal3 libunwind8 libupower-glib3 liburi-perl
      libutempter0 libuv1 libuv1-dev libv4l-0 libv4lconvert0 libva-drm2 libva-wayland2 libva-x11-2 libva2 libvariable-magic-perl libvdpau1 libvisio-0.1-1 libvisual-0.4-0 libvlc-bin libvlc5 libvlccore9
      libvncclient1 libvoikko1 libvte-2.91-0 libvte-2.91-common libvulkan1 libwacom-bin libwacom-common libwant-perl libwavpack1 libwayland-bin libwayland-client0 libwayland-cursor0 libwayland-dev libwayland-egl1
      libwayland-server0 libwbclient0 libwebkit2gtk-4.0-37 libwebpdemux2 libwebpmux3 libwebsockets-dev libwinpr2-2 libwmf-bin libwmf0.2-7 libwoff1 libwpd-0.10-10 libwpe-1.0-1 libwpebackend-fdo-1.0-1 libwps-0.4-4
      libwrap0 libx11-6 libx11-dev libx11-xcb1 libxapian30 libxapp1 libxatracker2 libxerces-c3.2 libxfont2 libxkbcommon-dev libxkbcommon-x11-0 libxkbcommon0 libxkbregistry0 libxml-libxml-perl libxml-libxslt-perl
      libxml-parser-perl libxml2 libxml2-dev libxmlsec1-nss libxpm4 libxslt1.1 libxstring-perl libxt6 libxtables12 libxxf86dga1 libyaml-libyaml-perl libyelp0 libz3-4 libz3-dev libzbar0 libzip4 libzmf-0.0-0 libzmq5
      libzstd1 libzvbi-common libzvbi0 licensecheck lightdm lintian linux-headers-amd64 linux-image-amd64 llvm llvm-runtime lm-sensors locales login logrotate logsave lsb-base lsd lshw lsof lvm2 lzip m4 mailutils
      mailutils-common malcontent malcontent-gui man-db manpages-fr mariadb-client mariadb-server mdadm memcached mesa-utils mesa-va-drivers mesa-vdpau-drivers mesa-vulkan-drivers mokutil mount mscompress
      mtp-tools muffin muffin-common nano nautilus-extension-gnome-terminal ncal ncdu ncurses-base ncurses-bin ncurses-term nemo nemo-data net-tools nethogs netpbm network-manager network-manager-gnome nfs-common
      ninja-build ntfs-3g ocl-icd-libopencl1 ocsinventory-agent odbcinst odbcinst1debian2 okular openssh-client openssh-server openssh-sftp-server openssl p11-kit p11-kit-modules packagekit packagekit-tools pandoc
      pandoc-data pango1.0-tools parted passwd pciutils perl perl-base perl-openssl-defaults perl-tk pidgin pidgin-data pinentry-curses pinentry-gnome3 pipewire pipewire-bin pixz pkg-config plasma-framework
      plymouth plymouth-label policykit-1 policykit-1-gnome poppler-utils postgresql-15 postgresql-client-15 ppp printer-driver-c2050 printer-driver-c2esp printer-driver-dymo printer-driver-fujixerox
      printer-driver-gutenprint printer-driver-hpcups printer-driver-pnm2ppa printer-driver-postscript-hp pristine-tar procps progress proj-bin ps2eps psmisc pstoedit pulseaudio pulseaudio-module-bluetooth
      pulseaudio-utils python3 python3-apt python3-brlapi python3-cairo python3-cffi-backend python3-cryptography python3-cups python3-cupshelpers python3-dbus python3-debian python3-distutils python3-gi
      python3-gi-cairo python3-gpg python3-ibus-1.0 python3-influxdb python3-kiwisolver python3-ldb python3-lib2to3 python3-louis python3-lxml python3-markupsafe python3-matplotlib python3-minimal
      python3-netifaces python3-numpy python3-pil python3-psutil python3-psycopg2 python3-pycurl python3-pyqt5 python3-pyqt5.sip python3-renderpm python3-reportlab python3-reportlab-accel python3-requests
      python3-samba python3-setproctitle python3-sip python3-smbc python3-software-properties python3-talloc python3-tdb python3-tinycss python3-tk python3-uno python3-yaml qml-module-org-kde-kconfig
      qml-module-org-kde-kirigami2 qml-module-org-kde-kquickcontrols qml-module-org-kde-kquickcontrolsaddons qml-module-org-kde-purpose qml-module-qt-labs-platform qml-module-qtgraphicaleffects
      qml-module-qtmultimedia qml-module-qtqml qml-module-qtquick-controls qml-module-qtquick-controls2 qml-module-qtquick-dialogs qml-module-qtquick-layouts qml-module-qtquick-particles2
      qml-module-qtquick-privatewidgets qml-module-qtquick-templates2 qml-module-qtquick-window2 qml-module-qtquick2 qml-module-qtwebengine qt5-gtk-platformtheme qtspeech5-speechd-plugin qtwayland5 realmd
      rhythmbox rhythmbox-plugin-cdrecorder rhythmbox-plugins ripgrep rpcbind rsync rsyslog rtkit ruby ruby-dev ruby-json rygel samba-common samba-common-bin samba-dsdb-modules samba-libs sane-utils scribus
      scribus-data seahorse sed shared-mime-info shim-signed shotwell shotwell-common simple-scan smbclient snapd socat software-properties-common software-properties-gtk sonnet-plugins sound-juicer
      speech-dispatcher speech-dispatcher-audio-plugins speech-dispatcher-espeak-ng spice-client-glib-usb-acl-helper squashfs-tools sshfs strace subversion sudo synaptic sysstat system-config-printer
      system-config-printer-common system-config-printer-udev systemd systemd-bootchart systemd-timesyncd sysvinit-utils tali tar tcl tcl-dev tcl8.6 tcl8.6-dev teckit telnet tesseract-ocr tesseract-ocr-eng
      tesseract-ocr-fra tesseract-ocr-osd tex-gyre texinfo texlive-base texlive-bibtex-extra texlive-binaries texlive-extra-utils texlive-font-utils texlive-fonts-extra texlive-fonts-extra-doc
      texlive-fonts-extra-links texlive-fonts-recommended texlive-fonts-recommended-doc texlive-formats-extra texlive-full texlive-games texlive-humanities texlive-humanities-doc texlive-lang-arabic
      texlive-lang-chinese texlive-lang-cjk texlive-lang-cyrillic texlive-lang-czechslovak texlive-lang-english texlive-lang-european texlive-lang-french texlive-lang-german texlive-lang-greek texlive-lang-italian
      texlive-lang-japanese texlive-lang-korean texlive-lang-other texlive-lang-polish texlive-lang-portuguese texlive-lang-spanish texlive-latex-base texlive-latex-base-doc texlive-latex-extra
      texlive-latex-extra-doc texlive-latex-recommended texlive-latex-recommended-doc texlive-luatex texlive-metapost texlive-metapost-doc texlive-music texlive-pictures texlive-pictures-doc texlive-plain-generic
      texlive-pstricks texlive-pstricks-doc texlive-publishers texlive-publishers-doc texlive-science texlive-science-doc texlive-xetex thunderbird timeshift tk tk-dev tk8.6 tk8.6-dev totem totem-plugins
      traceroute transmission-common transmission-gtk tree udev udisks2 uno-libs-private unzip upower ure usb-modeswitch util-linux util-linux-locales valac valac-bin vim vim-common vim-runtime vim-tiny vlc
      vlc-bin vlc-data vlc-plugin-access-extra vlc-plugin-base vlc-plugin-qt vlc-plugin-skins2 vlc-plugin-video-output wdiff wget whiptail wireshark wireshark-common wireshark-qt wodim wpasupplicant x11-apps
      x11-xkb-utils x11-xserver-utils xauth xbrlapi xdelta xdelta3 xdg-dbus-proxy xdg-desktop-portal xdg-desktop-portal-gtk xdg-user-dirs xdg-user-dirs-gtk xkbset xserver-xorg-core xserver-xorg-input-libinput
      xserver-xorg-input-wacom xserver-xorg-legacy xserver-xorg-video-amdgpu xserver-xorg-video-ati xserver-xorg-video-fbdev xserver-xorg-video-intel xserver-xorg-video-nouveau xserver-xorg-video-qxl
      xserver-xorg-video-radeon xserver-xorg-video-vesa xserver-xorg-video-vmware xterm xvfb xxd xz-utils yelp zenity zip zsh zsh-common
    Les paquets suivants seront mis à jour :
      adduser adwaita-icon-theme alsa-topology-conf alsa-ucm-conf apt-config-icons apt-transport-https aspell-en asymptote-doc autoconf automake autopoint autotools-dev base-files bash-completion bc bsdmainutils
      bzip2-doc ca-certificates cheese-common cm-super cm-super-minimal coinor-libcoinutils3v5 colord-data console-setup console-setup-linux context context-modules cryptsetup-run cups-common cups-server-common dc
      debconf debconf-i18n debhelper debian-archive-keyring debian-faq debian-keyring default-libmysqlclient-dev desktop-base dh-strip-nondeterminism dia-shapes dictionaries-common discover-data distro-info-data
      dkms dmraid dns-root-data dnsutils doc-debian docbook-xml dput duf emacsen-common exim4-config feynmf five-or-more fontconfig-config fonts-cantarell fonts-crosextra-caladea fonts-crosextra-carlito
      fonts-dejavu fonts-dejavu-core fonts-dejavu-extra fonts-ebgaramond fonts-ebgaramond-extra fonts-ipaexfont-gothic fonts-ipaexfont-mincho fonts-ipafont-gothic fonts-ipafont-mincho fonts-junicode
      fonts-liberation2 fonts-lmodern fonts-lyx fonts-noto-cjk fonts-noto-cjk-extra fonts-noto-color-emoji fonts-open-sans fonts-opensymbol fonts-sil-charis fonts-sil-gentiumplus fonts-sil-padauk fonts-tlwg-garuda
      fonts-tlwg-garuda-otf fonts-tlwg-kinnari fonts-tlwg-kinnari-otf fonts-tlwg-laksaman fonts-tlwg-laksaman-otf fonts-tlwg-loma fonts-tlwg-loma-otf fonts-tlwg-mono fonts-tlwg-mono-otf fonts-tlwg-norasi
      fonts-tlwg-norasi-otf fonts-tlwg-purisa fonts-tlwg-purisa-otf fonts-tlwg-sawasdee fonts-tlwg-sawasdee-otf fonts-tlwg-typewriter fonts-tlwg-typewriter-otf fonts-tlwg-typist fonts-tlwg-typist-otf
      fonts-tlwg-typo fonts-tlwg-typo-otf fonts-tlwg-umpush fonts-tlwg-umpush-otf fonts-tlwg-waree fonts-tlwg-waree-otf fonts-unfonts-core fonts-unfonts-extra fonts-urw-base35 foomatic-db-compressed-ppds
      foomatic-db-engine fortunes-min four-in-a-row fragmaster fwupd-amd64-signed gdal-data gdbm-l10n gdebi gdebi-core geany-plugin-prj geany-plugins-common gedit-plugins gedit-plugins-common geoip-database
      gir1.2-ayatanaappindicator3-0.1 gir1.2-caribou-1.0 gir1.2-clutter-1.0 gir1.2-gdesktopenums-3.0 gir1.2-gee-0.8 gir1.2-gkbd-3.0 gir1.2-gtkclutter-1.0 gir1.2-gtksource-3.0 gir1.2-gtksource-4
      gir1.2-gucharmap-2.90 gir1.2-handy-1 gir1.2-json-1.0 gir1.2-malcontent-0 gir1.2-nma-1.0 gir1.2-packagekitglib-1.0 gir1.2-peas-1.0 gir1.2-secret-1 gir1.2-timezonemap-1.0 gir1.2-totemplparser-1.0
      gir1.2-wnck-3.0 gist git-extras glib-networking-common gnome-accessibility-themes gnome-backgrounds gnome-games gnome-icon-theme gnome-keyring-pkcs11 gnome-mahjongg gnome-mines gnome-robots gnome-screenshot
      gnome-tetravex gnome-themes-extra gnome-themes-extra-data gnome-user-docs gsettings-desktop-schemas gsfonts gsfonts-x11 gstreamer1.0-clutter-3.0 gstreamer1.0-nice gtk2-engines gtk2-engines-murrine
      gutenprint-doc hitori hunspell-en-us hwdata hyphen-pt-pt iagno ifrench-gut im-config imagemagick imagemagick-6-common init installation-report intltool-debian inxi isc-dhcp-common iso-codes
      iso-flags-png-320x240 iw keyboard-configuration klibc-utils kpeople-vcard krb5-locales krita-data latex-cjk-chinese-arphic-bkai00mp latex-cjk-chinese-arphic-bsmi00lp latex-cjk-chinese-arphic-gbsn00lp
      latex-cjk-chinese-arphic-gkai00mp latex-cjk-japanese-wadalab latexdiff latexmk libaa1 libaccounts-glib0 libaec0 libaio1 libalgorithm-c3-perl libalgorithm-merge-perl libaliased-perl libamd2 libamtk-5-0
      libamtk-5-common libapache-pom-java libapache2-mod-dnssd libappstream-glib8 libarchive-cpio-perl libarpack2 libarray-intspan-perl libass9 libatk-adaptor libatk-bridge2.0-0 libatk-bridge2.0-dev libattr1
      libaudit-common libauthen-sasl-perl libb-hooks-endofscope-perl libblas3 libblockdev-fs2 libblockdev-loop2 libblockdev-part-err2 libblockdev-part2 libblockdev-swap2 libbluetooth3 libbrotli-dev libbrotli1
      libbs2b0 libbtparse2 libburn4 libbusiness-isbn-data-perl libbusiness-issn-perl libbytes-random-secure-perl libc-ares2 libc-l10n libcaca0 libcairomm-1.0-1v5 libcamd2 libcanberra-gtk3-0 libcanberra-gtk3-module
      libcapture-tiny-perl libcaribou-common libcaribou0 libccolamd2 libcddb2 libcdio-paranoia2 libcdparanoia0 libcdt5 libcgi-pm-perl libcgraph6 libcharls2 libcholmod3 libclass-accessor-perl libclass-c3-perl
      libclass-inspector-perl libclass-method-modifiers-perl libclass-singleton-perl libclucene-contribs1v5 libclutter-1.0-0 libclutter-1.0-common libclutter-gst-3.0-0 libclutter-gtk-1.0-0 libcogl-common
      libcolamd2 libcolord-gtk1 libcolord2 libcolorhug2 libcom-err2 libcommons-logging-java libcommons-parent-java libconfig-inifiles-perl libconfig-tiny-perl libconst-fast-perl libcontextual-return-perl
      libconvert-binhex-perl libcrack2 libcrypt-random-seed-perl libctf-nobfd0 libdata-compare-perl libdata-dpath-perl libdata-dump-perl libdata-optlist-perl libdata-uniqid-perl libdatetime-calendar-julian-perl
      libdatetime-format-strptime-perl libdatetime-locale-perl libdatetime-timezone-perl libdatrie-dev libdatrie1 libdbus-glib-1-2 libdbusmenu-glib4 libdbusmenu-gtk3-4 libdbusmenu-gtk4 libdc1394-25
      libdebconfclient0 libdebhelper-perl libdeflate0 libdevel-globaldestruction-perl libdevel-stacktrace-perl libdigest-hmac-perl libdist-checkconflicts-perl libdistro-info-perl libdmapsharing-3.0-2
      libdouble-conversion3 libdrm-common libdv4 libdvdnav4 libdynaloader-functions-perl libebml5 libegl1-mesa libegl1-mesa-dev libemail-date-format-perl libemf1 libencode-locale-perl libepoxy-dev libepoxy0
      liberror-perl libestr0 libeval-closure-perl libexception-class-perl libexif12 libexpat1 libexpat1-dev libexporter-tiny-perl libextutils-depends-perl libextutils-pkgconfig-perl libfaad2 libfastjson4
      libfile-basedir-perl libfile-desktopentry-perl libfile-dirlist-perl libfile-find-rule-perl libfile-homedir-perl libfile-listing-perl libfile-mimeinfo-perl libfile-sharedir-perl libfile-slurper-perl
      libfile-stripnondeterminism-perl libfile-touch-perl libfile-which-perl libfl2 libfont-afm-perl libfont-ttf-perl libfontembed1 libfreexl1 libfribidi-dev libfribidi0 libftdi1-2 libfwupd2 libgadu3 libgcab-1.0-0
      libgdata-common libgdata22 libgdl-3-5 libgdl-3-common libgee-0.8-2 libgee-0.8-dev libgegl-common libgeoclue-2-0 libgetopt-long-descriptive-perl libgif7 libgit-wrapper-perl libgitlab-api-v4-perl
      libgl1-mesa-glx libgladeui-common libglib2.0-data libglu1-mesa libgmp-dev libgmp10 libgmpxx4ldbl libgnome-games-support-1-3 libgnome-games-support-common libgnomekbd-common libgnomekbd8 libgoa-1.0-common
      libgoffice-0.10-10-common libgphoto2-l10n libgraphene-1.0-0 libgraphicsmagick++-q16-12 libgsf-1-common libgsm1 libgsound0 libgspell-1-common libgtk3-perl libgtksourceview-3.0-1 libgtksourceview-3.0-dev
      libgtksourceview-4-0 libgtksourceview-4-common libgucharmap-2-90-7 libgudev-1.0-0 libgusb2 libhandy-1-0 libhandy-1-dev libhdf5-hl-100 libhogweed6 libhtml-form-perl libhtml-format-perl
      libhtml-html5-entities-perl libhtml-tagset-perl libhtml-template-perl libhtml-tree-perl libhttp-daemon-perl libhttp-date-perl libhttp-negotiate-perl libhttp-parser2.9 libhttp-tiny-multipart-perl libidn2-0
      libidn2-dev libiec61883-0 libimage-magick-perl libimagequant0 libimport-into-perl libio-html-perl libio-prompter-perl libio-sessiondata-perl libio-socket-ssl-perl libio-string-perl libipc-run-perl
      libipc-run3-perl libipc-system-simple-perl libisl23 libisofs6 libiterator-perl libiterator-util-perl libjansson-dev libjansson4 libjbig0 libjbig2dec0 libjcat1 libjpeg-dev libjpeg62-turbo libjpeg62-turbo-dev
      libjs-jquery libjs-jquery-ui libjson-glib-1.0-0 libjson-glib-1.0-common libjson-maybexs-perl libjson-perl libjxr-tools libjxr0 libkeyutils1 libkf5attica5 libkf5bluezqt-data libkf5bluezqt6
      libkf5calendarevents5 libkf5codecs-data libkf5codecs5 libkf5i18n-data libkf5i18n5 libkf5itemviews-data libkf5itemviews5 libkf5jsapi5 libkf5kiontlm5 libkf5threadweaver5 libkf5widgetsaddons-data
      libkf5widgetsaddons5 libklibc liblab-gamut1 liblapack3 liblcms2-2 libldap-common liblightdm-gobject-1-0 liblingua-translit-perl liblist-compare-perl liblist-someutils-perl liblist-utilsby-perl
      liblog-any-adapter-screen-perl liblog-any-perl liblog-log4perl-perl liblognorm5 liblouis-data liblouisutdml-data libltc11 liblwp-mediatypes-perl liblz4-1 libmad0 libmagickwand-6.q16-6 libmail-sendmail-perl
      libmailtools-perl libmalcontent-0-0 libmanette-0.2-0 libmarkdown2 libmath-random-isaac-perl libmatroska7 libmd4c0 libmime-charset-perl libmime-lite-perl libmime-tools-perl libmime-types-perl libminiupnpc17
      libmjpegutils-2.1-0 libmm-glib0 libmodule-implementation-perl libmodule-runtime-perl libmoo-perl libmoox-aliases-perl libmoox-struct-perl libmp3lame0 libmpc3 libmpfr6 libmro-compat-perl libmtp-common
      libmysofa1 libmythes-1.2-0 libnamespace-autoclean-perl libnamespace-clean-perl libnatpmp1 libndp0 libnet-domain-tld-perl libnet-http-perl libnet-ip-perl libnet-netmask-perl libnet-smtp-ssl-perl
      libnet-snmp-perl libnetfilter-conntrack3 libnettle8 libnfnetlink0 libnghttp2-14 libnice10 libntlm0 libnumber-compare-perl libnumber-range-perl libnumbertext-data libobject-id-perl libofx7 libogg0 libonig5
      libopencore-amrnb0 libopencore-amrwb0 libopenmpt-modplug1 libopus0 liborc-0.4-0 libossp-uuid16 libpackage-stash-perl libpackagekit-glib2-18 libpam-runtime libpango1.0-0 libparams-validationcompiler-perl
      libparse-edid-perl libparse-recdescent-perl libpath-iterator-rule-perl libpath-tiny-perl libpathplan4 libpcaudio0 libpeas-common libperl4-corelibs-perl libpfm4 libpixman-1-0 libpixman-1-dev libplot2c2
      libpocketsphinx3 libpod-parser-perl libpolkit-qt5-1-1 libproc-daemon-perl libprotobuf-c1 libpurple-bin libpwquality-common libqhull8.0 libqmobipocket2 libqrcodegencpp1 libqt5concurrent5 libqt5help5
      libquazip5-1 libquvi-scripts-0.9 librecode0 libref-util-perl libregexp-common-perl libregexp-pattern-perl libreoffice-style-colibre libreoffice-style-elementary librttopo1 libsamplerate0 libsasl2-modules-db
      libsbc1 libseccomp2 libsecret-common libsensors-config libserd-0-0 libsigc++-2.0-0v5 libsignon-plugins-common1 libsignon-qt5-1 libsigsegv2 libsnappy1v5 libsnmp-base libsoap-lite-perl libsocket++1 libsonic0
      libsord-0-0 libsort-versions-perl libsoundtouch1 libsource-highlight-common libspecio-perl libspeex1 libspeexdsp1 libsratom-0-0 libsrtp2-1 libstemmer0d libstring-escape-perl libstring-shellquote-perl
      libsub-exporter-perl libsub-exporter-progressive-perl libsub-install-perl libsub-override-perl libsub-quote-perl libsuitesparseconfig5 libsuperlu5 libsys-hostname-long-perl libsz2 libtag1v5 libtag1v5-vanilla
      libtask-weaken-perl libtasn1-6 libtasn1-6-dev libtest-fatal-perl libtext-glob-perl libtext-roman-perl libtext-unidecode-perl libtext-wrapi18n-perl libthai-data libthai-dev libthai0 libtheora0
      libtie-ixhash-perl libtime-duration-perl libtimezonemap-data libtimezonemap1 libtinyxml2.6.2v5 libtirpc-common libtool libtotem-plparser-common libtry-tiny-perl libtss2-mu0 libtss2-sys1 libtss2-tcti-cmd0
      libtss2-tcti-device0 libtss2-tcti-mssim0 libtss2-tcti-swtpm0 libudfread0 libudisks2-0 libumfpack5 libuno-purpenvhelpergcc3-3 libuno-salhelpergcc3-3 libunoloader-java liburiparser1 libusb-1.0-0
      libusbredirhost1 libusbredirparser1 libutf8proc2 libuuid1 libvolume-key1 libwireshark-data libwnck-3-0 libwnck-3-common libwww-perl libx11-data libx11-protocol-perl libxaw7 libxcb-composite0 libxcb-cursor0
      libxcb-damage0 libxcb-dri2-0 libxcb-dri3-0 libxcb-glx0 libxcb-image0 libxcb-present0 libxcb-randr0 libxcb-render0 libxcb-render0-dev libxcb-res0 libxcb-shape0 libxcb-shm0 libxcb-shm0-dev libxcb-sync1
      libxcb-xfixes0 libxcb-xinerama0 libxcb-xinput0 libxcb-xkb1 libxcb-xtest0 libxcb-xv0 libxcb1 libxcb1-dev libxcursor-dev libxcursor1 libxdamage-dev libxdamage1 libxdelta2 libxext-dev libxext6 libxfixes-dev
      libxfixes3 libxft-dev libxft2 libxi-dev libxi6 libxinerama-dev libxinerama1 libxml-libxml-simple-perl libxml-namespacesupport-perl libxml-sax-base-perl libxml-sax-expat-perl libxml-sax-perl
      libxml-simple-perl libxml-twig-perl libxml-writer-perl libxml-xpathengine-perl libxmlrpc-lite-perl libxmlsec1 libxmu6 libxmuu1 libxnvctrl0 libxrandr-dev libxrandr2 libxrender-dev libxrender1 libxres1
      libxtst-dev libxtst6 libxv1 libxxhash0 libyajl2 libyaml-0-2 libyaml-perl libzephyr4 libzzip-0-13 lightdm-gtk-greeter lightsoff linux-base linux-libc-dev lmodern lsb-release lua-bitop lua-expat lua-lpeg
      lua-socket mailcap manpages manpages-dev mariadb-common mate-icon-theme mate-themes mawk media-types meld meson metacity-common mobile-broadband-provider-info mysql-common mythes-fr nemo-fileroller netbase
      netcat-traditional nettle-dev node-normalize.css openprinting-ppds orca os-prober pci.ids phonon4qt5-backend-vlc pocketsphinx-en-us poppler-data postgresql-client postgresql-client-common postgresql-common
      powermgmt-base printer-driver-all printer-driver-brlaser printer-driver-cjet printer-driver-foo2zjs printer-driver-foo2zjs-common printer-driver-m2300w printer-driver-ptouch printer-driver-pxljr
      printer-driver-sag-gdi proj-data publicsuffix purifyeps python-apt-common python-matplotlib-data python-tinycss2-common python3-bcrypt python3-blinker python3-bottle python3-bs4 python3-certifi
      python3-chardet python3-configobj python3-cycler python3-dateutil python3-debconf python3-debianbts python3-distro python3-distro-info python3-docker python3-fasteners python3-future python3-httplib2
      python3-idna python3-invoke python3-jwt python3-magic python3-mako python3-monotonic python3-nacl python3-oauthlib python3-pampy python3-paramiko python3-pexpect python3-pkg-resources python3-ply
      python3-ptyprocess python3-pyasn1 python3-pyatspi python3-pycryptodome python3-pygments python3-pyinotify python3-pyparsing python3-pysimplesoap python3-pysmi python3-pysnmp4 python3-pystache
      python3-reportbug python3-scour python3-setuptools python3-six python3-soupsieve python3-speechd python3-tinycss2 python3-tz python3-unidiff python3-urllib3 python3-webencodings python3-websocket
      python3-xapp python3-xdg python3-xlib qml-module-org-kde-bluezqt qml-module-org-kde-people qml-module-qt-labs-folderlistmodel qml-module-qt-labs-settings qml-module-qtqml-models2 qttranslations5-l10n
      quadrapassel rake readline-common rename reportbug rhythmbox-data rpl ruby-minitest ruby-net-telnet ruby-power-assert ruby-rubygems ruby-test-unit ruby-xmlrpc runit-helper sensible-utils sgml-base
      shim-helpers-amd64-signed shim-signed-common shim-unsigned sound-icons ssl-cert swell-foop systemd-sysv task-cinnamon-desktop task-desktop task-french task-french-desktop task-ssh-server tasksel tasksel-data
      terminator tex-common tipa tix totem-common tpm-udev ttf-bitstream-vera tuptime tzdata ucf unattended-upgrades update-inetd usb-modeswitch-data usb.ids usbutils uuid-dev va-driver-all vdpau-driver-all
      vlc-l10n vlc-plugin-notify vlc-plugin-samba vlc-plugin-video-splitter vlc-plugin-visualization wamerican wayland-protocols wfrench wireless-regdb x11-common x11-session-utils x11proto-core-dev x11proto-dev
      x11proto-input-dev x11proto-randr-dev x11proto-record-dev x11proto-scrnsaver-dev x11proto-xext-dev x11proto-xinerama-dev xapps-common xbitmaps xfonts-100dpi xfonts-75dpi xfonts-base xfonts-encodings
      xfonts-scalable xkb-data xorg xserver-common xserver-xorg xserver-xorg-input-all xserver-xorg-video-all yelp-xsl zenity-common zlib1g zlib1g-dev
    929 mis à jour, 0 nouvellement installés, 0 à enlever et 1541 non mis à jour.
    Il est nécessaire de prendre 877 Mo dans les archives.
    Après cette opération, 24,7 Mo d'espace disque supplémentaires seront utilisés.
    Souhaitez-vous continuer ? [O/n]


sudo apt full-upgrade
--------------------------

Ensuite, la commande qui procède à l'installation complète, en mettant à
jour tous les paquets et en supprimant les anciens::

    sudo apt full-upgrade

La durée de l’installation dépend essentiellement de la puissance de votre
machine et de la rapidité de la connexion.
Il se peut que le processus s’interrompe une ou plusieurs fois pour vous
poser des questions, selon votre configuration.

::

    Lecture des listes de paquets... Fait
    Construction de l'arbre des dépendances... Fait
    Lecture des informations d'état... Fait
    Calcul de la mise à jour... Fait
    Les paquets suivants ont été installés automatiquement et ne sont plus nécessaires :
      crda cryptsetup-run exfat-fuse fonts-ebgaramond freeglut3 gir1.2-ayatanaappindicator3-0.1 gir1.2-clutter-1.0 gir1.2-cogl-1.0 gir1.2-coglpango-1.0 gir1.2-gtkclutter-1.0 gist gnome-bluetooth-common
      guile-2.2-libs hddtemp libappstream-glib8 libarmadillo10 libart-2.0-2 libatk1.0-data libavdevice58 libavfilter7 libavformat58 libavresample4 libbpf0 libcbor0 libcfitsio9 libclang-cpp11 libclang1-11
      libcmark-gfm-extensions0 libcmark-gfm0 libcmis-0.5-5v5 libcolord-gtk1 libdap27 libdapclient6v5 libdleyna-connector-dbus-1.0-1 libdleyna-core-1.0-5 libdns-export1110 libepsilon1 libextutils-pkgconfig-perl
      libfam0 libfl2 libflac8 libfluidsynth2 libfmt7 libfwupdplugin1 libgdal28 libgdl-3-5 libgdl-3-common libgeocode-glib0 libgeos-3.9.0 libgetopt-long-descriptive-perl libgit2-1.1 libglew2.1 libgnome-bluetooth13
      libgs9-common libgssdp-1.2-0 libgtkspell3-3-0 libgupnp-1.2-0 libgweather-3-16 libgweather-common libhash-fieldhash-perl libicu67 libigdgmm11 libilmbase25 libindicator3-7 libisc-export1105 libjim0.79
      libjsoncpp24 libkf5idletime5 libkf5pulseaudioqt2 libldap-2.4-2 liblibreoffice-java libllvm11 libmalcontent-ui-0-0 libmbedcrypto3 libmbedtls12 libmbedx509-0 libmms0 libmoox-struct-perl libmpdec3
      libnautilus-extension1a libnetcdf18 libnetpbm10 libntfs-3g883 libnumber-range-perl libobject-id-perl libodbc1 libodbccr2 libofa0 libokular5core9 libopencolorio1v5 libopenexr25 liborcus-0.16-0
      liborcus-parser-0.16-0 libossp-uuid-perl libossp-uuid16 libpcre16-3 libpcre2-posix2 libpcre3-dev libpcre32-3 libpcrecpp0v5 libperl5.32 libphodav-2.0-0 libphodav-2.0-common libplacebo72 libpodofo0.9.7
      libpoppler102 libpostproc55 libprocps8 libproj19 libprotobuf-lite23 libpython3.9 libpython3.9-minimal libpython3.9-stdlib libqhull8.0 libqpdf28 libqrcodegencpp1 libqt5concurrent5 libquvi-0.9-0.9.3
      libquvi-scripts-0.9 librest-0.7-0 librygel-core-2.6-2 librygel-db-2.6-2 libsgutils2-2 libspdlog1 libsrt1.4-gnutls libswscale5 libtepl-5-0 libtesseract4 libtexlua53 libtinyxml2.6.2v5 libtracker-sparql-2.0-0
      libunoloader-java libusb-0.1-4 libvala-0.48-0 libvalacodegen-0.48-0 libwebsockets16 libwireshark14 libwiretap11 libwmf0.2-7 libwsutil12 libxmlb1 libyaml-cpp0.6 llvm-11 llvm-11-dev llvm-11-runtime
      llvm-11-tools lua-bitop lua-expat lua-json lua-lpeg lua-socket nautilus-extension-brasero odbcinst odbcinst1debian2 perl-modules-5.32 python3.9 python3.9-minimal ruby-json ruby2.7 ruby2.7-dev ruby2.7-doc
      telnet unattended-upgrades ure-java valac-0.48-vapi x11proto-input-dev x11proto-randr-dev x11proto-xinerama-dev
    Veuillez utiliser « sudo apt autoremove » pour les supprimer.
    Les paquets suivants seront ENLEVÉS :
      dleyna-server exfat-utils gedit-plugin-commander gedit-plugin-find-in-files gedit-plugin-translate libappindicator3-1 libc++1-11 libc++abi1-11 libgnome-desktop-3-19 libgnutlsxx28 libgranite5 libgs9 libgsasl7
      libgsl25 libgupnp-av-1.0-2 libgupnp-dlna-2.0-3 libjuh-java libjurt-java libmailutils7 libnfsidmap2 libopencv-core4.5 libopencv-imgcodecs4.5 libopencv-imgproc4.5 libopencv-videoio4.5 libridl-java
      librygel-renderer-2.6-2 librygel-server-2.6-2 libsemanage1 libsepol1-dev libtbb2 libwacom2 mariadb-client-10.5 mariadb-client-core-10.5 mariadb-server-10.5 mariadb-server-core-10.5
    Les NOUVEAUX paquets suivants seront installés :
      at-spi2-common avahi-utils ca-certificates-java catdoc cpp-12 cron-daemon-common dbus-bin dbus-daemon dbus-session-bus-common dbus-system-bus-common default-jre default-jre-headless
      evolution-data-server-common exfatprogs fonts-clear-sans fonts-comic-neue fonts-inter fonts-paratype fonts-roboto-slab fonts-sil-andika fonts-texgyre-math g++-12 gcc-11-base gcc-12 gcc-12-base gdal-plugins
      geocode-glib-common gir1.2-adw-1 gir1.2-amtk-5 gir1.2-camel-1.2 gir1.2-cscreensaver-1.0 gir1.2-ecal-2.0 gir1.2-edataserver-1.2 gir1.2-gnomedesktop-4.0 gir1.2-goa-1.0 gir1.2-graphene-1.0 gir1.2-gsound-1.0
      gir1.2-gtk-2.0 gir1.2-gtk-4.0 gir1.2-ical-3.0 gir1.2-soup-3.0 gir1.2-tepl-6 gnome-bluetooth-3-common gnome-bluetooth-common gnome-bluetooth-sendto gnome-characters gnome-logs gnome-remote-desktop
      gnome-session-bin grim guile-3.0-libs hexchat-lua html2text ibus-gtk4 id3 inetutils-telnet isympy-common isympy3 java-common lib2geom1.2.0 libabsl20220623 libadwaita-1-0 libaom3 libarmadillo11 libasan8
      libatk-wrapper-java libatk-wrapper-java-jni libavcodec59 libavdevice59 libavfilter8 libavformat59 libavif15 libavutil57 libberkeleydb-perl libbit-vector-perl libblosc1 libboost-dev libboost1.74-dev
      libbotan-2-19 libbox2d2 libbpf1 libc++1-14 libc++abi1-14 libcamel-1.2-64 libcarp-clan-perl libcbor0.8 libcfitsio10 libcjson1 libclang-cpp14 libclang1-14 libcloudproviders0 libcodec2-1.0 libcolord-gtk4-1
      libcrypt-rc4-perl libcurl3-nss libcurl4-nss-dev libdata-validate-ip-perl libdata-validate-uri-perl libdate-calc-perl libdate-calc-xs-perl libdate-manip-perl libdav1d6 libdaxctl1 libdebuginfod-common
      libdecor-0-0 libdecor-0-plugin-1-cairo libdee-1.0-4 libdeflate-dev libdigest-perl-md5-perl libdirectfb-1.7-7 libdmtx0b libduktape207 libecal-2.0-2 libedataserver-1.2-27 libencode-eucjpascii-perl
      libencode-perl libevent-core-2.1-7 libfeature-compat-class-perl libfeature-compat-try-perl libffi8 libflac12 libfluidsynth3 libfreeaptx0 libfreerdp-server2-2 libfreezethaw-perl libgav1-1 libgcc-12-dev
      libgdal32 libgeocode-glib-2-0 libgeos3.11.1 libgit2-1.5 libglew2.2 libglut3.12 libglvnd-core-dev libgnome-bg-4-2 libgnome-bluetooth-3.0-13 libgnome-bluetooth-ui-3.0-13 libgnome-desktop-3-20
      libgnome-desktop-4-2 libgnome-rr-4-2 libgnutlsxx30 libgprofng0 libgranite6 libgs-common libgs10 libgs10-common libgsasl18 libgsl27 libgssdp-1.6-0 libgssglue1 libgtk-4-1 libgtk-4-bin libgtk-4-common
      libgtksourceview-5-0 libgtksourceview-5-common libgumbo1 libgupnp-1.6-0 libgupnp-av-1.0-3 libgupnp-dlna-2.0-4 libgweather-4-0 libgweather-4-common libharfbuzz-subset0 libhtml-tokeparser-simple-perl
      libhwloc-plugins libhwloc15 libhwy1 libicu72 libidn12 libigdgmm12 libimath-3-1-29 libindirect-perl libio-interactive-perl libjavascriptcoregtk-4.1-0 libjaylink0 libjbig-dev libjcode-pm-perl libjemalloc2
      libjim0.81 libjs-sphinxdoc libjs-underscore libjsoncpp25 libjxl0.7 libkcolorpicker0 libkf5archive-data libkf5guiaddons-bin libkf5guiaddons-data libkf5i18nlocaledata5 libkf5itemmodels5 libkf5kcmutils-bin
      libkf5kcmutilscore5 libkf5newstuffcore5 libkf5prison5 libkf5prisonscanner5 libkf5pulseaudioqt3 libkf5runner5 libkf5syndication5abi1 libkimageannotator-common libkimageannotator0 libkseexpr-data libkseexpr4
      libkseexprui4 libkworkspace5-5 liblbfgsb0 liblc3-0 libldacbt-abr2 libldacbt-enc2 libldap-2.5-0 libldap-dev liblerc-dev liblerc4 libllvm14 libllvm15 liblrdf0 libmailutils9 libmalcontent-ui-1-1
      libmath-base85-perl libmbedcrypto7 libmbedtls14 libmbedx509-1 libmbim-glib4 libmbim-proxy libmldbm-perl libmozjs-102-0 libmujs2 libmutter-11-0 libnautilus-extension4 libndctl6 libneon27 libnet-ipv6addr-perl
      libnetaddr-ip-perl libnetcdf19 libnetpbm11 libnfsidmap1 libnftables1 libnma-gtk4-0 libntfs-3g89 libobject-pad-perl libodbc2 libodbccr2 libodbcinst2 libokular5core10 libole-storage-lite-perl libopenblas-dev
      libopenblas-pthread-dev libopenblas0 libopenblas0-pthread libopencolorio2.1 libopencv-core406 libopencv-imgcodecs406 libopencv-imgproc406 libopencv-videoio406 libopenexr-3-1-30 libopenh264-7 liborcus-0.17-0
      liborcus-parser-0.17-0 libpcre2-posix3 libperl5.36 libphodav-3.0-0 libphodav-3.0-common libpipewire-0.3-common libpkgconf3 libplacebo208 libpmem1 libpodofo0.9.8 libpoppler126 libportal-gtk3-1 libportal1
      libpostproc56 libproc2-0 libproj25 libprotobuf-lite32 libpystring0 libpython3-all-dev libpython3-dev libpython3.11 libpython3.11-dev libpython3.11-minimal libpython3.11-stdlib libqhull-r8.0 libqmi-glib5
      libqmi-proxy libqpdf29 libqrencode4 libqrtr-glib0 libqt5quickshapes5 libraqm0 librav1e0 libregexp-ipv6-perl libregexp-wildcards-perl librest-1.0-0 librist4 librnp0 libruby libruby3.1 librygel-core-2.8-0
      librygel-db-2.8-0 librygel-renderer-2.8-0 librygel-server-2.8-0 libsemanage2 libsepol-dev libsepol2 libset-intspan-perl libsgutils2-1.46-2 libsnapd-glib-2-1 libsocket6-perl libsoup-3.0-0 libsoup-3.0-common
      libsoup2.4-common libspa-0.2-bluetooth libspreadsheet-parseexcel-perl libspreadsheet-writeexcel-perl libsrt1.5-gnutls libssl3 libstdc++-12-dev libstring-crc32-perl libstring-license-perl libsvtav1enc1
      libswresample4 libswscale6 libsyntax-keyword-try-perl libsystemd-shared libtbb12 libtbbbind-2-5 libtbbmalloc2 libtepl-6-2 libtepl-common libtesseract5 libtexlua53-5 libtiff-dev libtiff6 libtiffxx6
      libtracker-sparql-3.0-0 libtsan2 libtspi1 libtss2-rc0 libtss2-tctildr0 libturbojpeg0 libunicode-map-perl libunity-protocol-private0 libunity-scopes-json-def-desktop libunity9 libunwind-14 liburing2
      libvala-0.56-0 libvalacodegen-0.56-0 libvpx7 libwacom9 libwebkit2gtk-4.1-0 libwebp-dev libwebp7 libwebsockets-evlib-ev libwebsockets-evlib-glib libwebsockets-evlib-uv libwebsockets17 libwireplumber-0.4-0
      libwireshark16 libwiretap13 libwmf-0.2-7 libwmflite-0.2-7 libwsutil14 libwww-mechanize-perl libx264-164 libx265-199 libxapp-gtk3-module libxcb-record0 libxcvt0 libxdo3 libxmlb2 libxs-parse-keyword-perl
      libxs-parse-sublike-perl libxsimd-dev libyaml-cpp0.7 libyuv0 libzimg2 libzstd-dev libzxing2 linux-compiler-gcc-12-x86 linux-headers-6.1.0-10-amd64 linux-headers-6.1.0-10-common linux-image-6.1.0-10-amd64
      linux-kbuild-6.1 llvm-14 llvm-14-dev llvm-14-linker-tools llvm-14-runtime llvm-14-tools lynx lynx-common mariadb-client-core mariadb-plugin-provider-bzip2 mariadb-plugin-provider-lz4
      mariadb-plugin-provider-lzma mariadb-plugin-provider-lzo mariadb-plugin-provider-snappy mariadb-server-core mesa-utils-bin mupdf-tools mutter-common node-clipboard node-prismjs nss-plugin-pem odt2txt
      openjdk-17-jre openjdk-17-jre-headless perl-modules-5.36 pipewire-pulse pkexec pkgconf pkgconf-bin polkitd polkitd-pkla power-profiles-daemon pv python3-all python3-all-dev python3-appdirs python3-attr
      python3-beniget python3-brotli python3-charset-normalizer python3-contourpy python3-cssselect python3-decorator python3-defusedxml python3-dev python3-fonttools python3-fs python3-gast
      python3-lazr.restfulclient python3-lazr.uri python3-libevdev python3-lz4 python3-mpmath python3-msgpack python3-packaging python3-pil.imagetk python3-pythran python3-pyudev python3-scipy python3-sympy
      python3-ufolib2 python3-ujson python3-wadllib python3-xlrd python3.11 python3.11-dev python3.11-minimal qml-module-org-kde-kcm qml-module-org-kde-kcmutils qml-module-org-kde-kitemmodels
      qml-module-org-kde-newstuff qml-module-org-kde-prison qml-module-org-kde-runnermodel qml-module-qtquick-shapes qtchooser qtwebengine5-dev-tools rpcsvc-proto ruby-sdbm ruby-webrick ruby3.1 ruby3.1-dev
      ruby3.1-doc sane-airscan systemsettings unicode-data unixodbc-common untex ure-java usrmerge util-linux-extra valac-0.56-vapi webp-pixbuf-loader wireplumber xapp-sn-watcher xcvt xwayland zstd
    Les paquets suivants seront mis à jour :
      accountsservice acl acpi aisleriot alsa-utils anacron apache2 apache2-bin apache2-data apache2-utils apparmor appstream apt apt-utils aspell asymptote at at-spi2-core avahi-daemon base-passwd bash biber
      bind9-dnsutils bind9-host bind9-libs binfmt-support binutils binutils-common binutils-x86-64-linux-gnu blueman bluez bluez-obexd bolt brasero brasero-cdrkit brasero-common bsdextrautils bsdutils bubblewrap
      busybox bzip2 ccze cdrdao cheese chktex chromium chromium-common chromium-l10n chromium-sandbox cifs-utils cinnamon cinnamon-common cinnamon-control-center cinnamon-control-center-data
      cinnamon-control-center-goa cinnamon-core cinnamon-desktop-data cinnamon-desktop-environment cinnamon-l10n cinnamon-screensaver cinnamon-session cinnamon-session-common cinnamon-settings-daemon cjs cmake
      cmake-data coinor-libcbc3 coinor-libcgl1 coinor-libclp1 colord coreutils cpio cpp cracklib-runtime cron cryptsetup cryptsetup-bin cryptsetup-initramfs cups cups-browsed cups-bsd cups-client cups-core-drivers
      cups-daemon cups-filters cups-filters-core-drivers cups-ipp-utils cups-ppdc curl dash dbus dbus-user-session dbus-x11 dconf-cli dconf-gsettings-backend dconf-service debianutils deja-dup devscripts dfc dia
      dia-common diffstat diffutils direnv dirmngr discover dmeventd dmidecode dmsetup dnsmasq-base doxygen dpkg dpkg-dev dracut-core duplicity dvdauthor dvisvgm dwz e2fsprogs efibootmgr eject enchant-2 eog
      espeak-ng-data evince evince-common exfat-fuse exif exim4-base exim4-daemon-light fakeroot falkon fdisk ffmpeg fig2dev file file-roller findutils firefox-esr firefox-esr-l10n-fr flameshot fontconfig
      fonts-texgyre fortune-mod fuse3 fwupd g++ galera-4 gawk gcc gconf-service gconf2 gconf2-common gcr gdb gdisk geany geany-common gedit gedit-common gedit-plugin-bookmarks gedit-plugin-bracket-completion
      gedit-plugin-character-map gedit-plugin-code-comment gedit-plugin-color-picker gedit-plugin-color-schemer gedit-plugin-draw-spaces gedit-plugin-git gedit-plugin-join-lines gedit-plugin-multi-edit
      gedit-plugin-session-saver gedit-plugin-smart-spaces gedit-plugin-synctex gedit-plugin-terminal gedit-plugin-text-size gedit-plugin-word-completion genisoimage geoclue-2.0 gettext gettext-base gh ghostscript
      gimp gimp-data gir1.2-accountsservice-1.0 gir1.2-atk-1.0 gir1.2-atspi-2.0 gir1.2-cinnamondesktop-3.0 gir1.2-cmenu-3.0 gir1.2-cogl-1.0 gir1.2-coglpango-1.0 gir1.2-cvc-1.0 gir1.2-freedesktop
      gir1.2-gdkpixbuf-2.0 gir1.2-ggit-1.0 gir1.2-glib-2.0 gir1.2-gnomedesktop-3.0 gir1.2-granite-1.0 gir1.2-gst-plugins-bad-1.0 gir1.2-gst-plugins-base-1.0 gir1.2-gstreamer-1.0 gir1.2-gtk-3.0 gir1.2-harfbuzz-0.0
      gir1.2-ibus-1.0 gir1.2-javascriptcoregtk-4.0 gir1.2-meta-muffin-0.0 gir1.2-nemo-3.0 gir1.2-nm-1.0 gir1.2-notify-0.7 gir1.2-pango-1.0 gir1.2-polkit-1.0 gir1.2-rb-3.0 gir1.2-soup-2.4 gir1.2-totem-1.0
      gir1.2-upowerglib-1.0 gir1.2-vte-2.91 gir1.2-webkit2-4.0 gir1.2-xapp-1.0 git git-man git-svn gjs gkbd-capplet glances glib-networking glib-networking-services gnome-2048 gnome-calculator gnome-chess
      gnome-control-center gnome-control-center-data gnome-desktop3-data gnome-disk-utility gnome-font-viewer gnome-keyring gnome-nibbles gnome-online-accounts gnome-paint gnome-settings-daemon
      gnome-settings-daemon-common gnome-software gnome-software-common gnome-sound-recorder gnome-sudoku gnome-system-monitor gnome-taquin gnome-terminal gnome-terminal-data gnome-user-share gnote gnupg
      gnupg-l10n gnupg-utils gnupg2 gpg gpg-agent gpg-wks-client gpg-wks-server gpgconf gpgsm gpgv graphviz grep grilo-plugins-0.3 grisbi grisbi-common groff-base grub-common grub-efi-amd64 grub-efi-amd64-bin
      grub-efi-amd64-signed grub-pc-bin grub2-common gsasl-common gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-libav gstreamer1.0-plugins-bad gstreamer1.0-plugins-base gstreamer1.0-plugins-good
      gstreamer1.0-plugins-ugly gstreamer1.0-pulseaudio gstreamer1.0-x gtk-update-icon-cache guile-2.2-libs gvfs gvfs-backends gvfs-common gvfs-daemons gvfs-fuse gvfs-libs gzip hdparm hexchat hexchat-common
      hexchat-perl hexchat-plugins hexchat-python3 hoichess hostname hplip hplip-data htop ibus ibus-data ibus-gtk ibus-gtk3 icu-devtools ifupdown imagemagick-6.q16 info init-system-helpers initramfs-tools
      initramfs-tools-core inkscape install-info intel-media-va-driver ipp-usb iproute2 iptables iputils-ping isc-dhcp-client ispell jmtpfs kaccounts-providers kactivities-bin kactivitymanagerd kbd kdeconnect
      kded5 keditbookmarks keyutils kinit kio kmod kpackagelauncherqml kpackagetool5 kpartx krita krita-gmic ksnip kwayland-data kwayland-integration latex-cjk-all latex-cjk-chinese latex-cjk-common
      latex-cjk-japanese latex-cjk-korean latex-cjk-thai lcdf-typetools ldap-utils less libaacs0 libaccountsservice0 libacl1 libalgorithm-diff-xs-perl libann0 libapparmor1 libappstream4 libapr1 libaprutil1
      libaprutil1-dbd-sqlite3 libaprutil1-ldap libapt-pkg-perl libapt-pkg6.0 libarchive-dev libarchive13 libargon2-1 libasan6 libasound2 libasound2-data libasound2-plugins libaspell15 libassuan0 libasyncns0
      libatk1.0-0 libatk1.0-dev libatkmm-1.6-1v5 libatomic1 libatopology2 libatspi2.0-0 libatspi2.0-dev libaudit1 libautovivification-perl libavahi-client3 libavahi-common-data libavahi-common3 libavahi-core7
      libavahi-glib1 libavahi-gobject0 libavahi-ui-gtk3-0 libayatana-appindicator3-1 libayatana-ido3-0.4-0 libayatana-indicator3-7 libb-hooks-op-check-perl libbabeltrace1 libbabl-0.1-0 libbdplus0 libbinutils
      libblkid-dev libblkid1 libblockdev-crypto2 libblockdev-utils2 libblockdev2 libbluray2 libboost-filesystem1.74.0 libboost-iostreams1.74.0 libboost-locale1.74.0 libboost-regex1.74.0 libboost-thread1.74.0
      libbrasero-media3-1 libbrlapi0.8 libbsd0 libbz2-1.0 libbz2-dev libc++1 libc-bin libc-dev-bin libc-devtools libc6 libc6-dbg libc6-dev libcairo-gobject-perl libcairo-gobject2 libcairo-perl
      libcairo-script-interpreter2 libcairo2 libcairo2-dev libcanberra-pulse libcanberra0 libcap-dev libcap-ng0 libcap2 libcap2-bin libcc1-0 libcdio-cdda2 libcdio19 libcdr-0.1-1 libcheese-gtk25 libcheese8
      libchromaprint1 libcinnamon-control-center1 libcinnamon-desktop4 libcinnamon-menu-3-0 libcjs0 libclass-c3-xs-perl libclass-xsaccessor-perl libclone-perl libclucene-core1v5 libcogl-pango20 libcogl-path20
      libcogl20 libcoin80c libcollada-dom2.5-dp0 libcommon-sense-perl libcpanel-json-xs-perl libcrypt-dev libcrypt-ssleay-perl libcrypt1 libcryptsetup12 libcscreensaver0 libctf0 libcups2 libcupsfilters1
      libcupsimage2 libcurl3-gnutls libcurl4 libcvc0 libdap27 libdapclient6v5 libdata-messagepack-perl libdate-simple-perl libdatetime-perl libdb5.3 libdbd-mysql-perl libdbi-perl libdbus-1-3 libdbus-1-dev
      libdconf1 libde265-0 libdebuginfod1 libdevel-callchecker-perl libdevel-caller-perl libdevel-lexalias-perl libdevel-size-perl libdevmapper-event1.02.1 libdevmapper1.02.1 libdiscover2 libdjvulibre21
      libdmraid1.0.0.rc16 libdpkg-perl libdrm-amdgpu1 libdrm-intel1 libdrm-nouveau2 libdrm-radeon1 libdrm2 libdvdread8 libdw1 libe-book-0.1-1 libedit2 libegl-dev libegl-mesa0 libegl1 libelf1
      libemail-address-xs-perl libenchant-2-2 libencode-eucjpms-perl libencode-hanextra-perl libencode-jis2k-perl libespeak-ng1 libetonyek-0.1-1 libevdev2 libevdocument3-4 libevent-2.1-7 libevview3-3 libexempi8
      libexiv2-27 libext2fs2 libfakeroot libfcgi-perl libfdisk1 libffi-dev libfftw3-double3 libfftw3-single3 libfido2-1 libfile-fcntllock-perl libflashrom1 libflatpak0 libflite1 libfontconfig-dev libfontconfig1
      libfontconfig1-dev libfreerdp2-2 libfreetype-dev libfreetype6 libfreetype6-dev libfstrm0 libfuse2 libfuse3-3 libfyba0 libgail-3-0 libgbm1 libgc1 libgcc-s1 libgck-1-0 libgconf-2-4 libgcr-base-3-1
      libgcr-ui-3-1 libgcrypt20 libgd3 libgdbm-compat4 libgdbm6 libgdcm3.0 libgdk-pixbuf-2.0-0 libgdk-pixbuf-2.0-dev libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common libgegl-0.4-0 libgeoip1 libgeos-c1v5 libgeotiff5
      libges-1.0-0 libgexiv2-2 libgfortran5 libgimp2.0 libgirepository-1.0-1 libgit2-glib-1.0-0 libgjs0g libgl-dev libgl1 libgl1-mesa-dri libgladeui-2-13 libglapi-mesa libgles-dev libgles1 libgles2
      libglib-object-introspection-perl libglib-perl libglib2.0-0 libglib2.0-bin libglib2.0-dev libglib2.0-dev-bin libglibmm-2.4-1v5 libglvnd-dev libglvnd0 libglx-dev libglx-mesa0 libglx0 libgme0 libgmic1
      libgnome-bluetooth13 libgnutls-dane0 libgnutls-openssl27 libgnutls28-dev libgnutls30 libgoa-1.0-0b libgoa-backend-1.0-1 libgoffice-0.10-10 libgomp1 libgpg-error0 libgpgme11 libgpgmepp6 libgphoto2-6
      libgphoto2-port12 libgpm2 libgpod-common libgpod4 libgranite-common libgranite-dev libgraphicsmagick-q16-3 libgrilo-0.3-0 libgs9-common libgsf-1-114 libgslcblas0 libgspell-1-2 libgssapi-krb5-2
      libgstreamer-gl1.0-0 libgstreamer-plugins-bad1.0-0 libgstreamer-plugins-base1.0-0 libgstreamer1.0-0 libgtk-3-0 libgtk-3-bin libgtk-3-common libgtk-3-dev libgtk-vnc-2.0-0 libgtkmm-3.0-1v5 libgts-0.7-5
      libgts-bin libgupnp-igd-1.0-4 libgutenprint-common libgutenprint9 libgvc6 libgvnc-1.0-0 libgvpr2 libgxps2 libharfbuzz-dev libharfbuzz-gobject0 libharfbuzz-icu0 libharfbuzz0b libhash-fieldhash-perl
      libhdf4-0-alt libhdf5-103-1 libheif1 libhfstospell11 libhpmud0 libhtml-parser-perl libhttp-message-perl libhunspell-1.7-0 libibus-1.0-5 libical3 libicu-dev libimage-magick-q16-perl libimobiledevice6
      libinput-bin libinput10 libio-pty-perl libip4tc2 libip6tc2 libipc-shareable-perl libipt2 libiptc0 libitm1 libjack-jackd2-0 libjavascriptcoregtk-4.0-18 libjson-c-dev libjson-c5 libjson-xs-perl libk5crypto3
      libkaccounts2 libkf5activities5 libkf5archive5 libkf5auth-data libkf5auth5 libkf5authcore5 libkf5bookmarks-data libkf5bookmarks5 libkf5completion-data libkf5completion5 libkf5config-bin libkf5config-data
      libkf5configcore5 libkf5configgui5 libkf5configwidgets-data libkf5configwidgets5 libkf5contacts-data libkf5contacts5 libkf5coreaddons-data libkf5coreaddons5 libkf5crash5 libkf5dbusaddons-bin
      libkf5dbusaddons-data libkf5dbusaddons5 libkf5declarative-data libkf5declarative5 libkf5doctools5 libkf5globalaccel-bin libkf5globalaccel-data libkf5globalaccel5 libkf5globalaccelprivate5 libkf5guiaddons5
      libkf5iconthemes-bin libkf5iconthemes-data libkf5iconthemes5 libkf5idletime5 libkf5jobwidgets-data libkf5jobwidgets5 libkf5js5 libkf5kcmutils-data libkf5kcmutils5 libkf5kexiv2-15.0.0 libkf5kiocore5
      libkf5kiofilewidgets5 libkf5kiogui5 libkf5kiowidgets5 libkf5kirigami2-5 libkf5notifications-data libkf5notifications5 libkf5package-data libkf5package5 libkf5parts-data libkf5parts-plugins libkf5parts5
      libkf5people-data libkf5people5 libkf5peoplebackend5 libkf5peoplewidgets5 libkf5plasma5 libkf5plasmaquick5 libkf5pty-data libkf5pty5 libkf5purpose-bin libkf5purpose5 libkf5quickaddons5 libkf5service-bin
      libkf5service-data libkf5service5 libkf5solid5 libkf5solid5-data libkf5sonnet5-data libkf5sonnetcore5 libkf5sonnetui5 libkf5textwidgets-data libkf5textwidgets5 libkf5wallet-bin libkf5wallet-data
      libkf5wallet5 libkf5waylandclient5 libkf5windowsystem-data libkf5windowsystem5 libkf5xmlgui-bin libkf5xmlgui-data libkf5xmlgui5 libkmlbase1 libkmldom1 libkmlengine1 libkmod2 libkpathsea6 libkrb5-3
      libkrb5support0 libksba8 libkwalletbackend5-5 liblangtag-common liblangtag1 liblcms2-utils libldap2-dev libldb2 liblept5 libleptonica-dev liblibreoffice-java liblilv-0-0 liblirc-client0 liblist-allutils-perl
      liblist-moreutils-xs-perl liblist-someutils-xs-perl liblocale-gettext-perl liblouis20 liblouisutdml-bin liblouisutdml9 liblsan0 libltdl-dev libltdl7 liblua5.2-0 liblua5.3-0 liblvm2cmd2.03 liblzma-dev
      liblzma5 libmagic-mgc libmagic1 libmagick++-6.q16-8 libmagickcore-6.q16-6 libmagickcore-6.q16-6-extra libmariadb-dev libmariadb-dev-compat libmariadb3 libmath-random-isaac-xs-perl libmaxminddb0 libmd0
      libmediaart-2.0-0 libmfx1 libmount-dev libmount1 libmouse-perl libmozjs-78-0 libmpeg2encpp-2.1-0 libmpg123-0 libmplex2-2.1-0 libmspub-0.1-1 libmtp-runtime libmtp9 libmuffin0 libmwaw-0.3-3 libncurses-dev
      libncurses5-dev libncurses6 libncursesw5-dev libncursesw6 libnemo-extension1 libneon27-gnutls libnet-cups-perl libnet-dbus-perl libnet-libidn-perl libnet-ssleay-perl libnewt0.52 libnftnl11 libnl-3-200
      libnl-genl-3-200 libnl-route-3-200 libnm0 libnma-common libnma0 libnotify4 libnspr4 libnss-mdns libnss-myhostname libnss-systemd libnss3 libnss3-tools libnuma1 libnumbertext-1.0-0 libodbc1 libogdi4.1
      libopengl-dev libopengl0 libopenjp2-7 libopenmpt0 libopenni2-0 libopenscenegraph161 libopenthreads21 libossp-uuid-perl libostree-1-1 libotr5 libp11-kit-dev libp11-kit0 libpackage-stash-xs-perl
      libpadwalker-perl libpam-gnome-keyring libpam-modules libpam-modules-bin libpam-systemd libpam0g libpango-1.0-0 libpango1.0-dev libpangocairo-1.0-0 libpangoft2-1.0-0 libpangomm-1.4-1v5 libpangoxft-1.0-0
      libpaper-utils libpaper1 libparams-classify-perl libparams-util-perl libparams-validate-perl libparted-fs-resize0 libparted2 libpcap0.8 libpci3 libpciaccess0 libpcre16-3 libpcre2-16-0 libpcre2-32-0
      libpcre2-8-0 libpcre2-dev libpcre3 libpcre3-dev libpcre32-3 libpcrecpp0v5 libpcsclite1 libpeas-1.0-0 libperlio-gzip-perl libperlio-utf8-strict-perl libpipeline1 libpipewire-0.3-0 libpipewire-0.3-modules
      libplist3 libplymouth5 libpng-dev libpng-tools libpng16-16 libpolkit-agent-1-0 libpolkit-gobject-1-0 libpoppler-cpp0v5 libpoppler-glib8 libpoppler-qt5-1 libpopt0 libpq-dev libpq5 libproc-processtable-perl
      libproxy-tools libproxy1v5 libpsl5 libpstoedit0c2a libptexenc1 libpulse-mainloop-glib0 libpulse0 libpulsedsp libpurple0 libpwquality1 libpython3-stdlib libqca-qt5-2 libqca-qt5-2-plugins libqt5core5a
      libqt5dbus5 libqt5designer5 libqt5gui5 libqt5multimedia5 libqt5multimedia5-plugins libqt5multimediagsttools5 libqt5multimediaquick5 libqt5multimediawidgets5 libqt5network5 libqt5opengl5 libqt5positioning5
      libqt5printsupport5 libqt5qml5 libqt5qmlmodels5 libqt5qmlworkerscript5 libqt5quick5 libqt5quickcontrols2-5 libqt5quickparticles5 libqt5quicktemplates2-5 libqt5quickwidgets5 libqt5sql5 libqt5sql5-sqlite
      libqt5svg5 libqt5test5 libqt5texttospeech5 libqt5waylandclient5 libqt5waylandcompositor5 libqt5webchannel5 libqt5webengine-data libqt5webengine5 libqt5webenginecore5 libqt5webenginewidgets5 libqt5widgets5
      libqt5x11extras5 libqt5xml5 libquadmath0 libqxp-0.0-0 librabbitmq4 libraptor2-0 librasqal3 libraw20 librdf0 libre-engine-re2-perl libre2-9 libreadline-dev libreadline8 libref-util-xs-perl
      libregexp-pattern-license-perl libreoffice-base-core libreoffice-calc libreoffice-common libreoffice-core libreoffice-draw libreoffice-gnome libreoffice-gtk3 libreoffice-help-common libreoffice-help-fr
      libreoffice-impress libreoffice-l10n-fr libreoffice-math libreoffice-writer librevenge-0.0-0 librhash0 librhythmbox-core10 librsvg2-2 librsvg2-bin librsvg2-common librsync2 librubberband2 libsane-common
      libsane-hpaio libsane1 libsasl2-2 libsasl2-dev libsasl2-modules libsdl-image1.2 libsdl1.2debian libsdl2-2.0-0 libsecret-1-0 libselinux1 libselinux1-dev libsemanage-common libsensors5 libsereal-decoder-perl
      libsereal-encoder-perl libserf-1-1 libshout3 libslang2 libsmartcols1 libsmbclient libsndfile1 libsndio7.0 libsnmp40 libsort-key-perl libsoup-gnome2.4-1 libsoup2.4-1 libsource-highlight4v5 libspa-0.2-modules
      libspandsp2 libspatialite7 libspectre1 libspeechd2 libsphinxbase3 libspice-client-glib-2.0-8 libspice-client-gtk-3.0-5 libsqlcipher0 libsqlite3-0 libsqlite3-dev libss2 libssh-gcrypt-4 libssh2-1 libssl-dev
      libstdc++6 libstring-copyright-perl libsub-identify-perl libsub-name-perl libsvn-perl libsvn1 libsynctex2 libsys-cpuaffinity-perl libsystemd0 libtalloc2 libtcl8.6 libtdb1 libteckit0 libterm-readkey-perl
      libtesseract-dev libtevent0 libtexlua53 libtexluajit2 libtext-bibtex-perl libtext-charwidth-perl libtext-csv-perl libtext-csv-xs-perl libtext-iconv-perl libtext-levenshteinxs-perl
      libtext-markdown-discount-perl libtext-xslate-perl libtime-moment-perl libtinfo6 libtirpc-dev libtirpc3 libtk8.6 libtotem-plparser18 libtotem0 libtsan0 libtss2-esys-3.0.2-0 libtype-tiny-perl
      libtype-tiny-xs-perl libubsan1 libudev1 libunbound8 libunicode-linebreak-perl libunicode-utf8-perl libunistring2 libuno-cppu3 libuno-cppuhelpergcc3-3 libuno-sal3 libunwind8 libupower-glib3 liburi-perl
      libutempter0 libuv1 libuv1-dev libv4l-0 libv4lconvert0 libva-drm2 libva-wayland2 libva-x11-2 libva2 libvariable-magic-perl libvdpau1 libvisio-0.1-1 libvisual-0.4-0 libvlc-bin libvlc5 libvlccore9
      libvncclient1 libvoikko1 libvte-2.91-0 libvte-2.91-common libvulkan1 libwacom-bin libwacom-common libwant-perl libwavpack1 libwayland-bin libwayland-client0 libwayland-cursor0 libwayland-dev libwayland-egl1
      libwayland-server0 libwbclient0 libwebkit2gtk-4.0-37 libwebpdemux2 libwebpmux3 libwebsockets-dev libwinpr2-2 libwmf-bin libwmf0.2-7 libwoff1 libwpd-0.10-10 libwpe-1.0-1 libwpebackend-fdo-1.0-1 libwps-0.4-4
      libwrap0 libx11-6 libx11-dev libx11-xcb1 libxapian30 libxapp1 libxatracker2 libxerces-c3.2 libxfont2 libxkbcommon-dev libxkbcommon-x11-0 libxkbcommon0 libxkbregistry0 libxml-libxml-perl libxml-libxslt-perl
      libxml-parser-perl libxml2 libxml2-dev libxmlsec1-nss libxpm4 libxslt1.1 libxstring-perl libxt6 libxtables12 libxxf86dga1 libyaml-libyaml-perl libyelp0 libz3-4 libz3-dev libzbar0 libzip4 libzmf-0.0-0 libzmq5
      libzstd1 libzvbi-common libzvbi0 licensecheck lightdm lintian linux-headers-amd64 linux-image-amd64 llvm llvm-runtime lm-sensors locales login logrotate logsave lsb-base lsd lshw lsof lvm2 lzip m4 mailutils
      mailutils-common malcontent malcontent-gui man-db manpages-fr mariadb-client mariadb-server mdadm memcached mesa-utils mesa-va-drivers mesa-vdpau-drivers mesa-vulkan-drivers mokutil mount mscompress
      mtp-tools muffin muffin-common nano nautilus-extension-gnome-terminal ncal ncdu ncurses-base ncurses-bin ncurses-term nemo nemo-data net-tools nethogs netpbm network-manager network-manager-gnome nfs-common
      ninja-build ntfs-3g ocl-icd-libopencl1 ocsinventory-agent odbcinst odbcinst1debian2 okular openssh-client openssh-server openssh-sftp-server openssl p11-kit p11-kit-modules packagekit packagekit-tools pandoc
      pandoc-data pango1.0-tools parted passwd pciutils perl perl-base perl-openssl-defaults perl-tk pidgin pidgin-data pinentry-curses pinentry-gnome3 pipewire pipewire-bin pixz pkg-config plasma-framework
      plymouth plymouth-label policykit-1 policykit-1-gnome poppler-utils postgresql-15 postgresql-client-15 ppp printer-driver-c2050 printer-driver-c2esp printer-driver-dymo printer-driver-fujixerox
      printer-driver-gutenprint printer-driver-hpcups printer-driver-pnm2ppa printer-driver-postscript-hp pristine-tar procps progress proj-bin ps2eps psmisc pstoedit pulseaudio pulseaudio-module-bluetooth
      pulseaudio-utils python3 python3-apt python3-brlapi python3-cairo python3-cffi-backend python3-cryptography python3-cups python3-cupshelpers python3-dbus python3-debian python3-distutils python3-gi
      python3-gi-cairo python3-gpg python3-ibus-1.0 python3-influxdb python3-kiwisolver python3-ldb python3-lib2to3 python3-louis python3-lxml python3-markupsafe python3-matplotlib python3-minimal
      python3-netifaces python3-numpy python3-pil python3-psutil python3-psycopg2 python3-pycurl python3-pyqt5 python3-pyqt5.sip python3-renderpm python3-reportlab python3-reportlab-accel python3-requests
      python3-samba python3-setproctitle python3-sip python3-smbc python3-software-properties python3-talloc python3-tdb python3-tinycss python3-tk python3-uno python3-yaml qml-module-org-kde-kconfig
      qml-module-org-kde-kirigami2 qml-module-org-kde-kquickcontrols qml-module-org-kde-kquickcontrolsaddons qml-module-org-kde-purpose qml-module-qt-labs-platform qml-module-qtgraphicaleffects
      qml-module-qtmultimedia qml-module-qtqml qml-module-qtquick-controls qml-module-qtquick-controls2 qml-module-qtquick-dialogs qml-module-qtquick-layouts qml-module-qtquick-particles2
      qml-module-qtquick-privatewidgets qml-module-qtquick-templates2 qml-module-qtquick-window2 qml-module-qtquick2 qml-module-qtwebengine qt5-gtk-platformtheme qtspeech5-speechd-plugin qtwayland5 realmd
      rhythmbox rhythmbox-plugin-cdrecorder rhythmbox-plugins ripgrep rpcbind rsync rsyslog rtkit ruby ruby-dev ruby-json rygel samba-common samba-common-bin samba-dsdb-modules samba-libs sane-utils scribus
      scribus-data seahorse sed shared-mime-info shim-signed shotwell shotwell-common simple-scan smbclient snapd socat software-properties-common software-properties-gtk sonnet-plugins sound-juicer
      speech-dispatcher speech-dispatcher-audio-plugins speech-dispatcher-espeak-ng spice-client-glib-usb-acl-helper squashfs-tools sshfs strace subversion sudo synaptic sysstat system-config-printer
      system-config-printer-common system-config-printer-udev systemd systemd-bootchart systemd-timesyncd sysvinit-utils tali tar tcl tcl-dev tcl8.6 tcl8.6-dev teckit telnet tesseract-ocr tesseract-ocr-eng
      tesseract-ocr-fra tesseract-ocr-osd tex-gyre texinfo texlive-base texlive-bibtex-extra texlive-binaries texlive-extra-utils texlive-font-utils texlive-fonts-extra texlive-fonts-extra-doc
      texlive-fonts-extra-links texlive-fonts-recommended texlive-fonts-recommended-doc texlive-formats-extra texlive-full texlive-games texlive-humanities texlive-humanities-doc texlive-lang-arabic
      texlive-lang-chinese texlive-lang-cjk texlive-lang-cyrillic texlive-lang-czechslovak texlive-lang-english texlive-lang-european texlive-lang-french texlive-lang-german texlive-lang-greek texlive-lang-italian
      texlive-lang-japanese texlive-lang-korean texlive-lang-other texlive-lang-polish texlive-lang-portuguese texlive-lang-spanish texlive-latex-base texlive-latex-base-doc texlive-latex-extra
      texlive-latex-extra-doc texlive-latex-recommended texlive-latex-recommended-doc texlive-luatex texlive-metapost texlive-metapost-doc texlive-music texlive-pictures texlive-pictures-doc texlive-plain-generic
      texlive-pstricks texlive-pstricks-doc texlive-publishers texlive-publishers-doc texlive-science texlive-science-doc texlive-xetex thunderbird timeshift tk tk-dev tk8.6 tk8.6-dev totem totem-plugins
      traceroute transmission-common transmission-gtk tree udev udisks2 uno-libs-private unzip upower ure usb-modeswitch util-linux util-linux-locales valac valac-bin vim vim-common vim-runtime vim-tiny vlc
      vlc-bin vlc-data vlc-plugin-access-extra vlc-plugin-base vlc-plugin-qt vlc-plugin-skins2 vlc-plugin-video-output wdiff wget whiptail wireshark wireshark-common wireshark-qt wodim wpasupplicant x11-apps
      x11-xkb-utils x11-xserver-utils xauth xbrlapi xdelta xdelta3 xdg-dbus-proxy xdg-desktop-portal xdg-desktop-portal-gtk xdg-user-dirs xdg-user-dirs-gtk xkbset xserver-xorg-core xserver-xorg-input-libinput
      xserver-xorg-input-wacom xserver-xorg-legacy xserver-xorg-video-amdgpu xserver-xorg-video-ati xserver-xorg-video-fbdev xserver-xorg-video-intel xserver-xorg-video-nouveau xserver-xorg-video-qxl
      xserver-xorg-video-radeon xserver-xorg-video-vesa xserver-xorg-video-vmware xterm xvfb xxd xz-utils yelp zenity zip zsh zsh-common
    1541 mis à jour, 477 nouvellement installés, 35 à enlever et 0 non mis à jour.
    Il est nécessaire de prendre 5 245 Mo dans les archives.
    Après cette opération, 3 430 Mo d'espace disque supplémentaires seront utilisés.
    Souhaitez-vous continuer ? [O/n]

17H38 =>

Redémarrer et profiter
=============================

Une fois que le terminal vous indique que l’installation est terminée,
il ne reste qu’à redémarrer.

Là encore, vous pouvez le faire via l’interface graphique ou avec la
commande sudo systemctl reboot.

Ce redémarrage sera à peine plus long, car Debian n’a pas de seconde
phase de préparation pour le nouveau système.

Après quoi, vous vous retrouverez sous Debian 12.

Vous le devinerez facilement en regardant l’interface de la version par
défaut : la version de GNOME faisant un grand bond entre l’ancien système
et le nouveau, la vue Activités affichera notamment le dock en bas.

Si vous avez installé un autre environnement de bureau ou souhaitez
simplement vérifier que tout s’est bien passé, entrez les deux commandes
suivantes dans un terminal::

    uname -r
    cat /etc/debian_version

La première affichera la version du noyau (6.1.0) et l’autre la version
du système (12.0).
