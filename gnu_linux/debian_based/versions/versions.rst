.. index::
   pair: Debian ; Versions

.. _debian_versions:

==========================================
Debian versions
==========================================

- https://fr.wikipedia.org/wiki/Debian
- https://fr.wikipedia.org/wiki/Debian#Chronologie_de_Debian_GNU/Linux
- https://fr.wikipedia.org/wiki/Historique_des_versions_de_Debian

.. toctree::
   :maxdepth: 3

   13/13
   12.1/12.1
   12/12
   11/11
   10/10
   9/9
