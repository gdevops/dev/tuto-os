

.. _linux_mint_def:

==========================================
GNU **Linux/mint** description
==========================================

.. seealso::

   - https://en.wikipedia.org/wiki/Linux_Mint

.. contents::
   :depth: 3



Definition
==========

.. seealso::

   - https://en.wikipedia.org/wiki/Linux_Mint


Linux Mint is a community-driven Linux distribution based on Ubuntu or Debian
that strives to be a 'modern, elegant and comfortable operating system which
is both powerful and easy to use'.

Linux Mint provides full out-of-the-box multimedia support by including some
proprietary software, such as multimedia codecs, and comes bundled with a variety
of free and open-source applications.

The project was created by Clément Lefèbvre and is being actively developed by
the Linux Mint Team and community

Définition en Français
========================

Linux Mint est un système d'exploitation GNU/Linux gratuit, créé en 2006 à
partir d'Ubuntu, lui-même basé sur Debian.

Il est conçu pour les ordinateurs individuels au même titre que Windows ou
Mac OS et s'affirme comme un « système moderne, élégant et confortable, à la
fois puissant et facile d'utilisation ».

Selon le site Alexa, Linux Mint est la distribution Linux la plus consultée
sur Internet, après Ubuntu et devant Debian.

Créé par Clément Lefèbvre, le projet est actuellement maintenu par l'équipe
de Linux Mint.

La devise de la distribution est « From freedom came elegance », ce qu'on peut
traduire en français par : « De la liberté vint (ou : naquit) l'élégance ».

Linux Mint suit les cycles de mises à niveau d'Ubuntu LTS. La dernière mise à
jour en date se nomme Linux Mint 19.3 « Tricia », sortie le 18 décembre 2019.
