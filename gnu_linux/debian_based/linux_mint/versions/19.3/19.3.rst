
.. index::
   pair: Linux-mint; 19.3 (2019-12-18)
   ! Tricia

.. _linux_mint_19_3:

============================================
GNU **Linux/mint 19.3 Tricia** (2019-12-18)
============================================

.. seealso::

   - https://blog.linuxmint.com/?p=3838
