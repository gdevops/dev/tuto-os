
.. index::
   pair: Linux-mint; Versions

.. _linux_mint_versions:

==========================================
GNU **Linux/mint** versions
==========================================

.. seealso::

   - https://github.com/linuxmint/Roadmap
   - https://fr.wikipedia.org/wiki/Linux_Mint#Tableau_des_versions


.. figure:: versions.png
   :align: center

   https://fr.wikipedia.org/wiki/Linux_Mint#Tableau_des_versions


.. toctree::
   :maxdepth: 3

   20.0/20.0
   19.3/19.3
   19.2/19.2
   19.1/19.1
   19/19
