
.. index::
   ! Linux-mint

.. _linux_mint:

==========================================
GNU **Linux/mint**
==========================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Linux_Mint
   - https://forums.linuxmint.com/index.php

.. toctree::
   :maxdepth: 3

   description/description
   versions/versions
