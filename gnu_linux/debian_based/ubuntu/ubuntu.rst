

.. index::
   ! Ubuntu


.. _ubuntu:

==========================================
Ubuntu
==========================================

.. seealso::

   - https://en.wikipedia.org/wiki/Ubuntu_(operating_system)


.. toctree::
   :maxdepth: 3

   versions/versions
