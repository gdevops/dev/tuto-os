

.. index::
   pair: Ubuntu ; versions


.. _ubuntu_versions:

==========================================
Ubuntu versions
==========================================

.. seealso::

   - https://en.wikipedia.org/wiki/Ubuntu#Releases


.. toctree::
   :maxdepth: 3

   18.04/18.04
