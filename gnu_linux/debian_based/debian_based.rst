
.. index::
   pair: Debian based ; OS
   ! Debian

.. _debian_based_os:

==========================================
GNU/Linux Debian based Operating Systems
==========================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Debian


.. toctree::
   :maxdepth: 3

   linux_mint/linux_mint
   ubuntu/ubuntu
   versions/versions
