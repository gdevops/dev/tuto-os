

.. index::
   pair: Fedora; Definition


.. _fedora_definition:

==========================================
Fedora definition
==========================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Fedora_%28GNU/Linux%29
   - https://fedoraproject.org/wiki/Red_Hat_contributions


Wikipedia definition
=====================

Fedora is a Linux distribution developed by the community-supported Fedora
Project and sponsored by Red Hat.

Fedora contains software distributed under various free and open-source
licenses and aims to be on the leading edge of such technologies.

**Fedora is the upstream source of the commercial Red Hat Enterprise Linux
distribution**.

Since the release of Fedora 21, three different editions are currently
available: Workstation, focused on the personal computer, Server for servers,
and Atomic focused on cloud computing.

As of February 2016, Fedora has an estimated 1.2 million users, including
Linus Torvalds, creator of the Linux kernel.
