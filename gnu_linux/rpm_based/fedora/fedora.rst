

.. index::
   pair: Fedora; OS


.. _fedora_os:

==========================================
Fedora
==========================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Fedora_%28GNU/Linux%29
   - https://x.com/fedora
   - https://getfedora.org/
   - https://fedoraproject.org/wiki/Red_Hat_contributions
   - https://x.com/fedorafr


.. figure:: Fedora_logo_and_wordmark.svg.png
   :align: center

.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
