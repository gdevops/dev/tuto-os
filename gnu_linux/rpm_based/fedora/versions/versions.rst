

.. index::
   pair: Fedora; Versions


.. _fedora_versions:

==========================================
Fedora versions
==========================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Fedora_%28GNU/Linux%29
   - https://en.wikipedia.org/wiki/Fedora_(operating_system)


.. toctree::
   :maxdepth: 3

   30/30
   29/29
