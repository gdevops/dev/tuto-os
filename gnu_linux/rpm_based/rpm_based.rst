

.. index::
   pair: RPM based ; OS


.. _rpm_based_os:

==========================================
GNU/Linux RPM based Operating Systems
==========================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Debian


.. toctree::
   :maxdepth: 3

   fedora/fedora
